#include <stdio.h>

int main(int agrc, char *argv[])
{
    char mystr[128], ch, *p;
    unsigned cnt;
    
    printf("Uma string ");
    fgets(mystr, sizeof(mystr), stdin);
    printf("Um caracter ");
    ch = fgetc(stdin);
    cnt = 0;

    for(p = mystr; *p; p++) {
        if(*p == ch) cnt++;
    
    }

    printf("%u Ocorrencias\n", cnt);

    return 0;

}
