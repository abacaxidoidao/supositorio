#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    float faren, celsius;

    faren = atoi(argv[1]);
    celsius = (faren - 32) / 1.8;

    printf("%.1f\n", celsius);

}
