#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int i, totalCed, valores[1000], valor, cedulas[] = { 100, 50, 20, 10, 5, 2, 1 };
 
    valor = atoi(argv[1]);

    for(i = 0; i < sizeof(cedulas)/sizeof(int); i++) {
        valores[i] = valor / cedulas[i];
        valor = valor % cedulas[i];
        
        }

    totalCed = valores[0] + valores[1] + valores[2] + valores[3] + valores[4] + valores[5] + valores[6] + valores[7];
        
    printf("%d\n", totalCed);    

}
