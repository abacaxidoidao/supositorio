#include <stdio.h>
#include <math.h>
#include <stdlib.h>

#define PI 3.141593 


int main(int agrc, char *argv[])
{
    int radiano;
    float ang1, seno, cose, tang;

    radiano = atoi(argv[1]);
    ang1 = radiano * (PI / 180);
    seno = sin(ang1);
    cose = cos(ang1);
    tang = tan(ang1);

    printf("Seno é: %.1f\n\nCosseno é: %.1f\n\nTangente é: %.1f\n\n", seno, cose, tang);

    return 0;

}
