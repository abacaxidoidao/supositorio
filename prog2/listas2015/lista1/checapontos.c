#include <stdio.h>
#include <math.h>

int main(int argc, char *argv[])
{
    int x, y;

    printf("Forneça o valor para x e y: ");
    scanf("%d %d", &x, &y);

    if (sqrt(x^2 + y^2) < 9) {
        printf("O ponto (%d,%d) não pertence ao círculo de raio 3 e centro (3,3)\n", x, y);
    }
    else {
        printf("O ponto (%d,%d) pertence ao círculo de raio 3 e centro (3,3)\n", x, y);
    
    }

    return 0;

}
