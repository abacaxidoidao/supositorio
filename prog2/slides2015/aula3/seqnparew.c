#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
    int a, b;

    a = atoi(argv[1]);
    b = atoi(argv[2]);
    
    if(a < b) {
        while(a <= b) {
            if(a % 2 == 0) {
                printf("%d\n", a);
                a++;

                }
            else { 
                ++a; 
                
                }

            }
        
        }
    else {
        while(b <= a) {
            if(b % 2 == 0) {
                printf("%d\n", b);
                b++;

                }
            else { 
                ++b; 
                
                }    

            }
        
        }
    
    return 0;
}
