#ifndef _MANIPARQ_H
#define _MANIPARQ_H
#define DIM 50

typedef struct data Data;

typedef struct cadastro Cadastro;

void printaMsg(char *);

char * leEntrada();

FILE * abreArquivo(char *);

int leLinha(char *, FILE *);

int leDados(Cadastro *, FILE *);

#endif
