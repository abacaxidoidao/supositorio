#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "manipArquivos.h"
#define ERR_MSG "Não foi possivel identificar o arquivo fornecido!"

struct data {
    int dia;
    int mes;
    int ano;
};

struct cadastro {
    char *nome;
    Data *nasc;
};

void printaMsg(char *msg) {
    printf("%s\n", msg);    
}

char * leEntrada() {
    int i = 1;

    char *arquivo = (char *)malloc(sizeof(char));
    while(scanf("%[^\n]", arquivo)) {
        i++;
        arquivo = (char *)realloc(arquivo, sizeof(char) * i);
    }
    return arquivo;
}

FILE * abreArquivo(char *arquivo) {
    FILE *file = fopen(arquivo, "r");
    if(file == NULL) {
        fprintf(stderr, "%s\n", ERR_MSG);
        exit(1);
    }

    return file;
}

int leLinha(char *linha, FILE *file) {
    int i;
    
    if(fgets(linha, 256, file) != NULL) {
        for(i = 0; i < strlen(linha); i++) {
            if(linha[i] == '\n')
                linha[i] = '\0';
        }
        return 1;
     }
    return 0;
}
    
int leDados(Cadastro *cadastro, FILE *file) {
    int dia, mes, ano, qtdClientes = 0, armazena;
    char linha[256], *token;

    fgets(linha, 256, file);
    int aux = atoi(linha);
    qtdClientes = aux;
    

    fgets(linha, 256, file);
    cadastro->nome = (char *)malloc(strlen(linha) + 1);
    strcpy(cadastro->nome, linha);
    printf("%s\n", cadastro->nome);

    while(armazena = leLinha(linha, file)) {
        if(linha[0] == '\0') 
            break;

        //le o nome
        strcpy(cadastro->nome, linha);
        //le a idade
        armazena = leLinha(linha, file);
        char *dup = strdup(linha);
        token = strtok(dup, " ");
        dia = atoi(token);
        cadastro->nasc->dia;
        token = strtok(NULL, " ");
        mes = atoi(token);
        cadastro->nasc->mes = mes;
        token = strtok(NULL, " ");
        ano = atoi(token);
        cadastro->nasc->ano = ano;

        cadastro++;
    }
    return qtdClientes;
}
