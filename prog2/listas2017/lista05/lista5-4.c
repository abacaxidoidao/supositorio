#include <stdio.h>

#define T 100

int checaOrdem(int vet[T], int n)
{
    int i, comparacao = 1;

    for(i = 0; i < n - 1; i++) {
        comparacao *= (vet[i] >= vet[i+1]);
    }
    if(comparacao) return 1;
    else {
        for(i = 0, comparacao = 1; i < n - 1; i++) {
            comparacao *= (vet[i] <= vet[i+1]);
        }
        if(comparacao) return 0;
    } 
    
    return 2; 
}

void imprimePos(int vet[T], int n)
{
    int i, distMax = 0, cond = checaOrdem(vet, n);

    for(i = 0; i < n - 1; i++) {
        if(cond == 1) {
            if(vet[i] - vet[i+1] > distMax) distMax = vet[i] - vet[i+1];
        } else if(vet[i+1] - vet[i] > distMax) distMax = vet[i+1] - vet[i];
    }

    printf("\n\nDistância máxima: %d\n", distMax);
}

int main(int argc, char *argv[])
{
    int compara, vet[T], n, i = 0, carac;

    printf("da o n: ");
    scanf("%d", &n);
    printf("vetor: ");
    while(i < n) {
        scanf("%d", &carac);
        vet[i] = carac;
        i++;
    }
    compara = checaOrdem(vet, n);

    if(compara == 0) printf("crescente\n");
    else if(compara == 1) printf("decrescente\n");
    else printf("fora de ordem\n");
    
    imprimePos(vet, n);

    return 0;
}
