#include <stdio.h>

#define T 100

int * inverte(int vet[T], int n)
{
    static int i, vetI[T];
    //int* vetI;
    // vetI = (int*)malloc(sizeof(int)*T);
    for(i = 0; i < n; i++) {
        vetI[i] = vet[n-1-i];
        //printf("%d ", vetI[i]);
    }

    return vetI;
}

int comparaVet(int vet[T], int n)
{
    int i, *vetI;

    vetI = inverte(vet, n);

    for(i = 0; i < n; i++) {
        if(vet[i] == vetI[i]) return 1;
    }

    return 0;
}

int main(int argc, char *argv[])
{
    int n, i = 0, vet[T], carac;
    
    printf("poe o n: ");
    scanf("%d", &n);
    printf("poe o vetor: ");
    while(i < n) {
        scanf("%d", &carac);
        vet[i] = carac;
        i++;
    }

    if(comparaVet(vet, n)) printf("ha!\n");
    else printf(":(\n");

    return 0;
}
