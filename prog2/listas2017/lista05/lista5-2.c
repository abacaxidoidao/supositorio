#include <stdio.h>
#include <string.h>

#define T 100

void soma(int vet1[T], int vet2[T], int n1, int n2)
{
    int i, flag, vetR[T];

    if(n1 > n2) {
        for(flag = 0; flag < n2; flag++) {
            vetR[flag] = vet1[flag] + vet2[flag];
        }
        for(flag = n2; flag < n1; flag++) {
            vetR[flag] = vet1[flag];
        }
        for(i = 0; i < n1; i++) {
            printf("%d ", vetR[i]);
        }
    } else {
        for(flag = 0; flag < n1; flag++) {
            vetR[flag] = vet1[flag] + vet2[flag];
        }
        for(flag = n1; flag < n2; flag++) {
            vetR[flag] = vet2[flag];
        }
        for(i = 0; i < n2; i++) {
            printf("%d ", vetR[i]);
        }
    }
    
    printf("\n");
}


int main(int argc, char *argv[])
{
    int vet1[T], vet2[T], n1, n2, i = 0, j = 0, carac1, carac2;

    
    printf("Forneca a qntd de car do vetor 1 e 2: ");
    scanf("%d %d", &n1, &n2);

    printf("vet1: ");
    while(i != n1) {
        scanf("%d", &carac1);
        vet1[i] = carac1;
        i++;
    }

    printf("vet2: ");
    while(j != n2) {
        scanf("%d", &carac2);
        vet2[j] = carac2;
        j++;
    }

    soma(vet1, vet2, n1, n2);

    return 0;
}
