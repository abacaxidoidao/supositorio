#include <stdio.h>
#include <string.h>

#define T 100

int maior(int vet[T], int n)
{
    int i, flag = 0;

    for(i = 0; i < n; i++) {
        if(vet[i] > flag) flag = vet[i];
    }

    return flag;
}



int menor(int vet[T], int n)
{
    int i, flag = maior(vet, n);

    for(i = 0; i < n; i++) {
        if(vet[i] < flag) flag = vet[i];
    }

    return flag;
}




int main(int argc, char *argv[])
{
    int vet[T], n, carac, i = 0;
    
    printf("Forneca qnts caracteres terá o vetor: ");
    scanf("%d", &n);
    printf("vetor: ");
    while(i < n) {
        scanf("%d", &carac);
        vet[i] = carac;
        i++;
    }
//    int oi[11] = {3, 4, 5, 3, 2, 6, 5, 8, 9};
    
    printf("%d\n", menor(vet, n));

    return 0;
}
