#include <stdio.h>

#define T 100

int comparaNum(int vet1[T], int vet2[T], int n2)
{
    int i, flag = 0;

    for(i = 0; i < n2; i++) {
        if(vet1[i] == vet2[i]) flag += 1;
    }
}


int main(int argc, char *argv[])
{
    int vet1[T], vet2[T], n1, n2, num, i;


    printf("Tamanho do vetor 1: ");
    scanf("%d", &n1);
    printf("Vetor 1: ");
    for(i = 0; i < n1; i++) {
        scanf("%d", &vet1[i]);
    }
    printf("Tamanho do vetor 2: ");
    scanf("%d", &n2);
    printf("Vetor 2: ");
    for(i = 0; i < n2; i++) {
        scanf("%d", &vet2[i]);
    }


    return 0;
}

