#include <stdio.h>

#define T 100

int procuraX(int vet[T], int n, int x)
{
    int i;

    for(i = 0; i < n; i++) {
        if(vet[i] == x) return i;
    }
    return -1;
}

void retiraX(int vet[T], int n, int x) 
{
    int i = 1, j;

    while(i >= 0) {
        i = procuraX(vet, n, x);
        if(i >= 0) {
            for(j = i; j < n - 1; j++) {
                vet[j] = vet[j + 1];
            }
            n--;
        }
    }
    for(j = 0; j < n; j++) {
        printf("%d ", vet[j]);
    } printf("\n");
}

int main(int argc, char *argv[])
{
    int v[T], i = 0, num, x, n;

    printf("n: ");
    scanf("%d", &n);
    printf("x: ");
    scanf("%d", &x);
    printf("v: ");
    while(i < n) {
        scanf("%d", &num);
        v[i] = num;
        i++;
    }

    retiraX(v, n, x);

    return 0;
}
