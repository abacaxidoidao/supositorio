#include <stdio.h>

int divisor(int num1, int num2)
{
    int flag = 1, i, div;

    if(num2 > num1) i = num1;
    else i = num2;
    
    while(i > 0) {
        if(!((num1 % i) || (num2 % i))) {
            num1 /= i;
            num2 /= i;
            div = i;
            flag *= div;
        }
        i--;
       //printf("%d %d %d %d %d ..\n", div, i, num1, num2, flag);   
    }

    return flag;
}

int main(int argc, char *argv[])
{
    int num1, num2;

    printf("coe: ");
    scanf("%d %d", &num1, &num2);
    
    printf("%d\n", divisor(num1, num2));

    return 0;
}
