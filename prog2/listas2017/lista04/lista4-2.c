#include <stdio.h>

void seq(int a, int b)
{

    for(; a < b; a++) {
        printf("%d ", a);    
    }

    printf("\n");
}

int main(int argc, char *argv[])
{
    int a, b;
 
    printf("Forneca dois numeros: ");
    scanf("%d %d", &a, &b);

    seq(a, b);
    
    return 0;   
}
