#include <stdio.h>

int fatorialAux(int);

int fatorial(void)
{
    int num;

    do {
        printf("coe: ");
        scanf("%d", &num);

    } while(num < 0); 

    return fatorialAux(num);
}

int fatorialAux(int num)
{

    if(num == 0) {
        return 1;
    }

    return num * fatorialAux(num-1);
}

int main(int argc, char *argv[])
{

    printf("%d\n", fatorial());

    return 0;
}
