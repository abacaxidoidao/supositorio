#include <stdio.h>

int mdc(int num1, int num2)
{
    if(num1 > num2) return mdc(num1-num2, num2);
    else if(num1 < num2) return mdc(num1, num2-num1);
    else return num1;

}

int main(int argc, char *argv[])
{
    int num1, num2;

    printf("coe: ");
    scanf("%d %d", &num1, &num2);

    printf("%d\n", mdc(num1, num2));

    return 0;
}
