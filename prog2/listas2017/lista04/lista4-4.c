#include <stdio.h>

int descobretam(int num)
{
    int flag = 0;
    
    while(num) {
        num = num / 10;
        flag++;    
    }

    return flag;
}

int potenciaDez(int pote) 
{    
    int i, valor = 1;

    for(i = 1; i <= pote; i++) {
        valor *= 10;
    }
    
    return valor;
}

int main(int argc, char *argv[])
{
    int tamNum, tamSuf, num, suf;

    printf("coe: ");
    scanf("%d %d", &num, &suf);
    
    tamSuf = descobretam(suf);

    do {
        tamNum = descobretam(num);
        int j = tamNum - 1;

        num = num - (num / potenciaDez(j)) * potenciaDez(j);
        printf("%d\n", num);
        j--;
        
        if(num == suf) {
            printf("Sim\n");
            return 0;
       
       }   

    } while(tamNum > tamSuf);
    
    printf("Não\n");

    return 0;
}
