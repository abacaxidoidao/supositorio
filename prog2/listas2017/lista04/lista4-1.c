#include <stdio.h>

int somar(int x, int y)
{
    return x + y;
    
}

int subt(int x, int y)
{
    return x - y;    
}

int mult(int x, int y)
{
    return x * y;    
}

int div(int x, int y)
{
    return x / y;    
}

int main(int argc, char *argv[])
{
    int opcao, x, y;

    do {
        
        printf("1- Somar, 2 - Subtrair, 3 - Multiplicar, 4 - Dividir, 5 - Sair\n");
        scanf("%d", &opcao);

        switch(opcao) {
            case 1:
                printf("Forneca 2 numeros: ");
                scanf("%d %d", &x, &y);

                printf("%d\n", somar(x, y));
                break;
            case 2:
                printf("Forneca 2 numeros: ");
                scanf("%d %d", &x, &y);

                printf("%d\n", subt(x, y));
                break;
            case 3:
                printf("Forneca 2 numeros: ");
                scanf("%d %d", &x, &y);

                printf("%d\n", mult(x, y));
                break;
            case 4:
                printf("Forneca 2 numeros: ");
                scanf("%d %d", &x, &y);

                printf("%d\n", div(x, y));
                break;
            case 5:
                continue;
        }   
    } while(opcao != 5);
}

