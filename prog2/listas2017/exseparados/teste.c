#include <stdio.h>
#include <string.h>

int elem(char caracter,char *string) //Funcao que diz se um caracter é elemento de uma string
{
    int i, tamanho;

    tamanho = strlen(string);

    for(i = 0; i < tamanho; i++) {
        if(caracter == string[i]) return 1; //Retornará 1 (verdadeiro) se um caracter pertencer a uma string
    
    }

    return 0; //Retornamos 0 (falso) caso não pertenca
    
}

void funcaotirarepetido(char *vetor)
{
    int i, t, tamanho; //Definimos um t que será o index de newVet, para que não haja posicionamento de caracteres em indexes aleatórios

    tamanho = strlen(vetor);

    char newVet[tamanho]; //newVet terá no MÁXIMO o mesmo tamanho que o vetor passado

    for(i = 0, t = 0; i < tamanho; i++) {
        if(i == 0) { //Quando i for zero, ele adicionará em newVet pois ele com certeza não pertencerá ao vetor e irá incrementar t
            newVet[t] = vetor[i];
            t++;
        }
        else if(elem(vetor[i], newVet)) { //Se ele for elemento, então não incrementa t, para não pular indexes
            continue;           
        }  
        else if(!elem(vetor[i], newVet)) { //Se for elemento, adiciona à string e incrementa t
            newVet[t] = vetor[i];
            t++;
        }

    }
    for(i = 0; i < t; i++) {//Printamos os caracteres 1 por 1 para que nao imprima trash memory
        printf("%c", newVet[i]);
    }
    printf("\n");
}


int main(int argc, char *argv[]) 
{
    char vetor[200];

    printf("Forneca uma string: ");
    scanf("%[^\n]", vetor);

    funcaotirarepetido(vetor);

    return 0;
}
