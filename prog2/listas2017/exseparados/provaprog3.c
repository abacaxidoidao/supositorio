#include <stdio.h>
#include <string.h>

#define T 201

char vogais[10] = {'a', 'e', 'i', 'o', 'u', 'A', 'E', 'I', 'O', 'U'};

int funcaoelem(char caracter, char *string) 
{
    int i, tamanho;

    tamanho = strlen(string);
    
    for(i = 0; i < tamanho; i++) {
        if(caracter == string[i]) return 1;
    
    }

	return 0;

}

void funcaocontaLetra(char *palavras) 
{
	int i, v, c, tamanho;

	tamanho = strlen(palavras);

    for(i = 0, v = 0, c = 0; i < tamanho; i++) {
        if(funcaoelem(palavras[i], vogais)) v++;
        else if((palavras[i] >= 'a' && palavras[i] <= 'z') || (palavras[i] >= 'A' && palavras[i] <= 'Z')) c++;
        else continue;

    }

	printf("Total de vogais: %d, total de consoantes: %d\n", v, c);

}


int main(int argc, char *argv[])
{
	char palavra[T];

	printf("Forneça uma string: ");
	scanf("%[^\n]", palavra);

	funcaocontaLetra(palavra);

}





