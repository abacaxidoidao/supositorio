#include <stdio.h>

int main(int argc, char *argv[])
{
    int val1, val2, coringa1, coringa2, participantes, tentativas, flag, jogador, contador;

    coringa1 = 15;
    coringa2 = 37;
    jogador = 1;

    printf("Forneca o numero de participantes: ");
    scanf("%d", &participantes);
    printf("Numero maximo de tentativas: ");
    scanf("%d", &tentativas);

    do {
        printf("Jogador %d:\n", jogador);
        
        contador = 0;
        flag = 0;

        do {

            printf("Forneca dois numeros inteiros no intervalo de 0 a 100: ");
            scanf("%d %d", &val1, &val2);

            
            if((val1 == coringa1 && val2 == coringa2) || (val2 == coringa1 && val1 == coringa2)) {
                flag++;
                contador = 2;
                break;
            }
            else if((val1 != coringa1 && val2 != coringa2) || (val1 != coringa2 && val2 != coringa1)) {
                flag++;
            }
            else {
                flag++;
                contador++;
            }

            if(contador == 2) break;
        } while(flag < tentativas && contador != 2);

        jogador++;
    } while(jogador <= participantes);

    return 0;
}
