#include <stdio.h>

int main(int agrc, char *argv[])
{
    int n;

    int cont = 0;
    int max = 0;

    float acc = 0;

    while(scanf("%d", &n)) {
        if(!n) break;
        if(n) {
            acc = acc + n;
            cont++;
        }
        if(n > max) max = n;
    
    printf("%d %f\n", max,  acc/cont);

    }


    return 0;
}
