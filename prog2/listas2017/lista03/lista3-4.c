#include <stdio.h>
#include <string.h>

int main(int agrc, char *argv[])
{
    int i, n, max, min, par, impar;

    scanf("%d", &n);
    
    int num[n];
    par = 0;
    impar = 0;

    for(i = 0; i < n; i++) {
        scanf("%d", &num[i]);
    }

    max = 0;
    for(i = 0; i < n; i++) {
        if(num[i] > max) max = num[i];
    }
    //printf("%d\n", max);

    min = max;
    for(i = 0; i < n; i++) {
        if(num[i] < min) min = num[i];
        if(!(num[i] % 2)) par++;
        else impar++;
    }
    printf("%d %d %d %d\n", max, min, par, impar);

    

    /*for(j = 0; j < n; j++) {
        printf("%d ", num[j]);
    }
    printf("\n");*/

    return 0;
}
