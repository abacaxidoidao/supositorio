#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void intervalo(int n, int m)
{
    int *lista, i, j;

    lista = (int *) malloc ((m - n) * sizeof(int));
    n += 1;
    
    for(j = 0; n < m; n++) {
        if(!(n % 3 && n % 4 && n % 7)) {
            lista[j] = n;
            j++;
        }
    }

    for(i = 0; i < j; i++) {
        printf("%d ", lista[i]);
    }
    printf("\n");

}

int main(int argc, char *argv[])
{
    int n, m;

    printf("Forneca: ");
    scanf("%d %d", &n, &m);
    intervalo(n, m);

    return 0;
}
