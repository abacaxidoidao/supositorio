#include <stdio.h>

int main(int argc, char *argv[]) 
{
    int c, letra;

    letra = 0;

    while((c = getchar()) != '.') if((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) letra++;
    
    printf("%d\n", letra);

    return 0;
}


