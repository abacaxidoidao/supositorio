#include <stdio.h>

int main(int argc, char *argv[])
{
    int n, m, div, flag;

    scanf("%d %d", &n, &m);

    n += 1;
    for(; n < m; n++) {
        for(flag = 1, div = 0; flag <= n; flag++) {
            if(!(n % flag)) div++;
        }
        if(div == 2) printf("%d ", n);
    }
    
    printf("\n");

    return 0;
}
