#include <stdio.h>

int main(int argc, char *argv[])
{
    int num, i, flag;

    scanf("%d", &num);

    for(i = 1, flag = 0; i <= num; i++) {
        if(!(num % i)) flag++;
    }
    if(flag == 2) printf("Primo\n");
    else printf("Não primo\n");

    return 0;
}   
