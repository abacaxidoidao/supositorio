#include <stdio.h>
#include <string.h>

#define M 100

int funcaoehPrefix(char *string1, char *string2) 
{
    int i;

    for(i = 0; i < strlen(string1); i++) {
        if(string1[i] != string2[i]){
            return 1;
         }
    }
    
    return 0;

}

int funcaoehInfix(char *string3, char *string4) 
{
    int i, j, difString, flag;

    difString = strlen(string4) - strlen(string3);
    

    for(i = 0; i <= difString; i++) {
        for(j = 0; j < strlen(string3); ++j) {
            flag = j;
            if(string3[j] != string4[j+i]) break;
            if(flag == (strlen(string3) - 1)) return 0;

        }
    }

    return 1;

}



int main(int argc, char *argv[]) 
{
    int ehPrefix, ehInfix;
    char string1[M], string2[M], string3[M], string4[M];
    

    printf("Insira duas strings para verificar se uma eh prefixo da outra: ");
    scanf("%s %s", string1, string2);

    ehPrefix = funcaoehPrefix(string1, string2);

    if(ehPrefix == 0) printf("Eh prefixo\n");
    else printf("Nao eh prefixo\n");

    printf("Insira duas strings para verificar se uma eh infixo da outra: ");
    scanf("%s %s", string3, string4);

    ehInfix = funcaoehInfix(string3, string4);

    if(strlen(string3) > strlen(string4)) {
        printf("A primeira nao deve ser maior que a segunda\n");

    }
    else if(ehInfix == 0) {
        printf("Eh infixo\n");

    }
    else {
        printf("Nao eh infixo\n");
    }
    return 0;
}
