#include <stdio.h>

int main(int argc, char *argv[])
{
	int areas[] = {10, 11, 13, 14, 20};
	char nome[] = "Leo";
	char nome_todo[] = {
		'L', 'e', 'o',
		' ', 'F', '.', ' ',
		'S', 'h', 'a', 'w', '\0'
	};
	// areas[0] = 100;
	// areas[1] = 200;
	// nome[0] = 'D';
	// nome_todo[0] = 'D';
	// areas[0] = nome[0];

	// WARNING: On some systems you may have to change the %ld in this cod to a %u since it will use unsigned ints

	printf("The size of an int: %ld\n", sizeof(int));
	printf("The size of areas (int[]): %ld\n", sizeof(areas));
	printf("The number of ints in areas: %ld\n", sizeof(areas) / sizeof(int));
	printf("The first area is %d, the 2nd %d.\n", areas[0], areas[1]);
	printf("The size of a char: %ld\n", sizeof(char));
	printf("The size of a name (char[]): %ld\n", sizeof(nome));
	printf("The number of chars: %ld\n", sizeof(nome) / sizeof(char));
	printf("The size of nome_todo (char[]): %ld\n", sizeof(nome_todo));
	printf("The number of chars: %ld\n", sizeof(nome_todo) / sizeof(char));
	printf("nome=\"%s\" and nome_todo=\"%s\"\n", nome, nome_todo);

	return 0;
	
	
	
	
}
