#include <stdio.h>

int main(int argc, char *argv[])
{
	int i = 0;

	if (argc == 1) {
		printf("A função só tem um argumento.\n");
	} else if (argc > 1 && argc < 4) {
		printf("Você passou %d argumentos, são eles:\n", argc-1);


		for (i = 0; i < argc; i++) {
			printf("%s ", argv[i]);
		}
		printf("\n");
	} else {
		printf("Você tem muitos argumentos.\n");
	}
	return 0;


}
