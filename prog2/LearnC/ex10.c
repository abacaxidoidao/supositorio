#include <stdio.h>
#include <ctype.h>

int main(int argc, char *argv[])
{
	if(argc != 2) {
		printf("ERROR: You need one argument. \n");
		// this is how to abort a program
		return 1;
	}
	
	int i = 0;
	for(i = 0; argv[1][i] != '\0'; i++) {
		char letter = argv[1][i];
		char letras = tolower(letter);

		switch(letras) {
			case 'a':
				printf("%d: %c\n", i, letter);
				break;

			case 'e':
				printf("%d: %c\n", i, letter);
				break;
			
			case 'i':
				printf("%d: %c\n", i, letter);
				break;
			case 'o':
				printf("%d: %c\n", i, letter);
				break;

			case 'u':
				printf("%d: %c\n", i, letter);
				break;
			
			case 'y':
				if(i > 2) {
					printf("%d: 'Y'\n", i);
				//break;
			}
				break;
			default:
			    printf("%d: %c is not a vowel\n", i, letter);
		}
	}
	
	
}
