#include <stdio.h>
#include <string.h>

int main ()
{
	const char src[] = "tt";
	char dest[50];

	printf("Before memcpy dest = %s\n", dest);
	memcpy(dest, src, strlen(src));
	printf("After memcpy dest = %s\n", dest);
	//printf("teste %c\n", src[15]);

	return 0;
	
}
