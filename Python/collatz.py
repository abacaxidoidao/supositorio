#!/bin/python

def collatz(number):
    if number // 2 == 0:
        endResult = number // 2
        print(endResult)
        return endResult
    endResult = (number * 3) + 1
    return endResult

def chama_collatz():
    while True:
        num = input()
        try:
            num = int(num)
        except:
            print('Nao eh numero')
            continue
        retorno = collatz(num)
        print(retorno)
        if retorno == 1:
            break
        
    print(retorno)

chama_collatz()