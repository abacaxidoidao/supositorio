#!/bin/python

import functools as fnt

def clist(*args):
    list = []
    i = 0

    for arg in args:
        list.insert(i, arg)
        i += 1

    return list

def add(*args):
    sum = 0

    for arg in args:
        sum += arg
    
    return sum

def negate(arg):
    return (arg * -1)

def compose(*functions):
    def compose2(fn1, fn2):
        return lambda x: fn1(fn2(x))

    return fnt.reduce(compose2, functions, lambda x: x)


    


