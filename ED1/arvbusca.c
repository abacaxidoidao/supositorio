#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arvbusca.h"

struct arvbusca {
  int info;
  ArvB *esq, *dir;
};

ArvB * arv_criavazia(void) {
  return NULL;
}

ArvB *arv_insere(int info, ArvB *a) {
  if (!a) {
    a = (ArvB *)malloc(sizeof(ArvB));
    a->esq = a->dir = NULL;
  } else if (info < a->info)
    a->esq = arv_insere(info, a->esq);
  else if (info > a->info) 
    a->dir = arv_insere(info, a->dir);

  return a;
}

int busca_maior(ArvB *a) {
  while (a->dir != NULL)
    a = a->dir;
  return a->info;
}

int arv_remove(ArvB *a, int info) {
  ArvB *aux;
  if (a == NULL)
    return 0; 
// árvore vazia ou não achou
// Localiza o elemento a ser removido
  else if (info < (a)->info)
    return(arv_remove(a->esq, info));
  else if (info > (a)->info)
    return(arv_remove(a->dir, info));
  else { 
    if (((a)->esq==NULL) && ((a)->dir==NULL)) {
      free(a);
      a = NULL;
      return 1;
    }
    else if ((a->esq) == NULL) {
      aux = a;
      a = a->dir;
      free(aux);
      return 1;
    }
    else if(a->dir == NULL) {
      aux = a->esq;
      a = a->esq;
      free(aux);
      return 1;
    }
    else {
      a->info = busca_maior(a->esq);
      return arv_remove(a->esq, a->info);
    }
  }
}

void arv_libera(ArvB *a) {
  if (!a)
    return;
  if (a->esq)
    arv_libera(a->esq);
  if (a->dir)
    arv_libera(a->dir);
  
  free(a);
}

int main() {

  return 0;
}