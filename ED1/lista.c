#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "lista.h"

#define ALOCA(TYPE, SIZE) (TYPE*)malloc((SIZE) * sizeof(TYPE))

struct tipoitem {
    char *nome;
    int matricula;
    char *endereco;
    TipoItem *prox;
};

struct tipolista {
    TipoItem *fst;
    TipoItem *lst;
    int tam;
};

TipoLista * InicializaLista(void) { 
    TipoLista *newList = ALOCA(TipoLista, 1);

    newList->fst = NULL;
    newList->lst = NULL;
    newList->tam = 0;

    return newList;
}

void lista_vazia(TipoLista *lista) {
    if(!lista) {
        fprintf(stderr, "A lista passada nao existe, abortando programa!");
        exit(1);
    }
}

void celula_vazia(TipoItem *celula) {
    if(!celula) {
        fprintf(stderr, "O item passado nao existe, abortando programa!");
        exit(1);
    }
}

void Insere(TipoItem *aluno, TipoLista *lista) {
    lista_vazia(lista);
    celula_vazia(aluno);

    if(!lista->fst) {
        lista->fst = aluno;
        lista->lst = aluno;
        lista->tam = 1;
        aluno->prox = NULL;

        return;
    }

    aluno->prox = lista->fst;
    lista->fst = aluno;
    lista->tam += 1;
}

TipoItem * procura_por_mat(TipoLista *lista, int mat) {
    lista_vazia(lista);

    TipoItem *aluno = lista->fst;

    while(aluno) {
        if(aluno->matricula == mat) {
            return aluno;
        }
        aluno = aluno->prox;
    }
    fprintf(stderr, "Aluno nao achado!\n");
    return NULL;
}

TipoItem * Retira(TipoLista *lista, int mat) {
    TipoItem *aluno;
    if((aluno = procura_por_mat(lista, mat))) {
        TipoItem *proximo = lista->fst->prox;
        TipoItem *anterior = lista->fst;
        
        if(!proximo) {
                lista->fst = lista->lst = NULL;
                return aluno;
            }

        while(anterior) {
            if(proximo == aluno) {
                anterior->prox = proximo->prox;
                return aluno;
            }
            anterior = anterior->prox;
            proximo = proximo->prox;
        }
    }
    return NULL;
}

void Imprime(TipoLista *lista) {
    lista_vazia(lista);

    TipoItem *aluno = lista->fst;

    while(aluno != NULL) {
        printf("Nome do aluno: %s\n", aluno->nome);
        printf("Matricula: %d\n", aluno->matricula);
        printf("Endereco: %s\n", aluno->endereco);

        aluno = aluno->prox;
    }
}

void libera_um(TipoItem *item) {
    celula_vazia(item);

    free(item->nome);
    free(item->endereco);
    free(item);
}

TipoLista * Libera(TipoLista *lista) {
    lista_vazia(lista);

    TipoItem *proximo = lista->fst->prox;
    TipoItem *anterior = lista->fst;

    if((!proximo) && anterior) {
        libera_um(anterior);
        free(lista);
        return NULL;
    }

    while(anterior) {
        libera_um(anterior);        
        anterior = proximo;
        proximo = proximo->prox;   

        if(!proximo) {
            libera_um(anterior);
            break;
        }
    }

    free(lista);


    return NULL;
}

TipoItem * InicializaTipoItem(char *nome, int matricula, char *endereco) {
    if(!(nome || endereco)) {
        fprintf(stderr, "Nome ou endereco invalidos, abortando programa!");
        exit(1);
    }
    if(matricula <= 0) {
        fprintf(stderr, "Matricula invalida, abortando programa!");
        exit(1);
    }

    TipoItem *newItem = ALOCA(TipoItem, 1);

    // newItem->nome = nome;
    newItem->nome = (char *)malloc(sizeof(char) * (strlen(nome)+1));
    strcpy(newItem->nome, nome);
    
    newItem->matricula = matricula;
    
    // newItem->endereco = endereco;
    newItem->endereco = (char *)malloc(sizeof(char) * (strlen(endereco)+1));
    strcpy(newItem->endereco, endereco);

    newItem->prox = NULL;

    return newItem;
}