#include <stdio.h>
#include <stdlib.h>
#include "arvore.h"

struct arv {
    char info;
    Arv *e;
    Arv *d;
};

Arv * arv_criavazia(void) {
    return NULL;
}

Arv * arv_cria(char c, Arv *e, Arv *d) {
    Arv *arvores = (Arv *)malloc(sizeof(Arv));

    arvores->info = c;
    arvores->e = e;
    arvores->d = d;

    return arvores;
}

int arv_vazia(Arv *a) {
    return a->info == 0;
}

int arv_pertence(Arv *a, char c) {
    return 
        a->info == c
        || (a->e && arv_pertence(a->e, c))
        || (a->d && arv_pertence(a->d, c));
}

void arv_imprime(Arv *a) {
    if (!a)
        return;
    
    printf("< %c", a->info);

    if (a->e) 
        arv_imprime(a->e);
    if (a->d) 
        arv_imprime(a->d);
    printf(">");
}

char info(Arv *a) {
    return a->info;
}

Arv * arv_pai(Arv *a, char c) {
    if (!a)
        return NULL;
    if ((a->e && (info(a->e) == c)) || (a->d && (info(a->d) == c)))
        return a;
    Arv *r;
    return ((r = arv_pai(a->e, c)) ? r : arv_pai(a->d, c));
}

int folhas(Arv *a) {
    if (!a)
        return 0;
    if (a->e || a->d)
        return folhas(a->e) + folhas(a->d);

    return 1;
}

int ocorrencias(Arv *a, char c) {
    if (!a)
        return 0;

        return (info(a) == c) 
        + (a->e && ocorrencias(a->e, c)) 
        + (a->d && ocorrencias(a->d, c));
}

Arv * arv_libera(Arv * a) {
    if (!a)
        return NULL;
    
    if (a->e)
        arv_libera(a->e);
    if (a->d)
        arv_libera(a->d);
    
    free(a);
    return NULL;
}