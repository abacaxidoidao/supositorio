#include <stdio.h>
#include <stdlib.h>

#define R 5.00
#define PI 3.14
#define ALOC(TYPE, SIZE) ((TYPE*)malloc((SIZE) * sizeof(TYPE)))

// void * alocaEspaco(int tam, void *generico) {
//     generico = calloc(tam,sizeof(int *)); //para inicializar com 0's
    
//     return generico;
// }

void calc_esfera(float r, float *area, float *volume) {
    *area = 4 * PI * r * r;
    *volume = (4 * PI * r * r * r) / 3; 
    
    printf("Área = %f\nVolume = %f\n", *area, *volume);
}

int * converte_vec(int n, float *iniVec){
    int i, *finalVec;

    finalVec = ALOC(int, n);
    for(i=0;i<n;i++){
        finalVec[i]=(int)iniVec[i];
    }
    return finalVec;
}

void printf_pf(int n, float *vet) {
    int i;

    for(i=0;i<n;i++)
        printf("%f",*(vet+i));
    printf("\n");

}

void printf_pi(int n, int *vet){
    int i;

    for(i=0;i<n;i++){
        printf("%d ",*(vet+i));
    }
    printf("\n");
}

float * negativos(int n, float *vet) {
    int i,j=0;
    float *nVet;

    nVet=(float *)calloc(n,sizeof(float));

    for(i=0;i<n;i++){
        if(vet[i]<0){
            nVet[j]=vet[i];
            j++;
        }
    }
    
    return nVet;
}

void inverte(int n,float *vet){
    int i,j;
    float *rVet;

    rVet=(float *)calloc(n,sizeof(float));

    for(i=n,j=0;i>=0;--i,j++){
        rVet[j]=vet[i-1];
    }
    printf_pf(n,rVet);
    free(rVet);
}

int cmpfunc(const void *a, const void *b) {
   return (**(int**)a - **(int**)b);
}

int ** inverte2(int n, int *vet){
    int i;

    int **finalVet = (int **)malloc(n*sizeof(int *));
     for(i=0;i<n;i++){
        finalVet[i]=(vet + i);
    }
    
    // for(i=0;i<n;i++){
    //     finalVet[i]=(int *)malloc(n*sizeof(int));
    // }
    
    printf_pi(5, *finalVet);
    printf("b\n\n\n\n"); 

    qsort(finalVet, n, sizeof(int *), cmpfunc);

    // printf_pi(5, *finalVet);

    printf("ordenado: ");
    for (int i=0; i<n; i++) printf("%d ", *(finalVet[i]));
    printf("\n");

    printf("a\n\n\n\n"); 
    return finalVet;
}

int main(){
    float vet[5] = {1,-4,7,-1,6};
    int vet2[5] = {1,-4,7,-1,6};
    
    // float area,vol;
    // calc_esfera(R,&area,&vol);

    // float *nVet=negativos(5,vet);
    // printf_pf(5,nVet);
    // free(nVet);

    // inverte(5,vet);
    int **vec=inverte2(5, vet2);
    // printf_pi(5, vet2);
    // printf_pi(5, *vec);
    printf("\n");
    return 0;
}
