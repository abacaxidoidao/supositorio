#include <stdio.h>
#include <stdlib.h>

#define ALOCA(TYPE, SIZE) (TYPE*)malloc((SIZE) * sizeof(TYPE))

typedef struct item Celula;
typedef struct sentinela Lista;

struct item {
    int elem;
    Celula *prox;
};
struct sentinela {
    Celula *fst;
    Celula *lst;
    int tam;
};

Lista * inicializa_lista(void) {
    Lista *sentinela = ALOCA(Lista, 1);

    sentinela->fst = NULL;
    sentinela->lst = NULL;
    sentinela->tam = 0;

    return sentinela;
}

Celula * inicializa_celula(void) {
    Celula *item = ALOCA(Celula, 1);
    
    printf("Insira o elemento dessa celula: ");
    scanf("%d", &item->elem);
    item->prox = NULL;

    return item;
}

void lista_eh_vazia(Lista *lista) {
    if(lista->fst == NULL) {
        printf("Lista vazia, abortando programa!\n");
        exit(1);
    }
}

int eh_cres(Lista *lista) {
    lista_eh_vazia(lista);
    
    Celula *aux;

    for(aux = lista->fst; aux != NULL; aux = aux->prox) {
        if(aux->elem > aux->prox->elem) {
            return 0;
        }
    }
    return 1;
}

int eh_decres(Lista *lista) {
    lista_eh_vazia(lista);
    
    Celula *aux;

    for(aux = lista->fst; aux != NULL; aux = aux->prox) {
        if(aux->elem < aux->prox->elem) {
            return 0;
        }
    }
    return 1;
}

int esta_ordenada(Lista *lista) {
    lista_eh_vazia(lista);
    
    int ehCres = eh_cres(lista);
    int ehDecres = eh_decres(lista);

    return (ehCres || ehDecres);
}



int pos_eh_valida(Lista *lista, int pos) {
    lista_eh_vazia(lista);
    
    do {
        printf("Qual posicao voce quer inserir o novo item?\nPosicao VALIDA desejada: ");
        scanf("%d", &pos);
    } while((pos < 0) || (pos >= (lista->tam)));

    return pos;
}

Celula * insere_na_pos(Lista *lista) {  
    Celula *novoItem = ALOCA(Celula, 1);
    Celula *aux;
    
    int pos = pos_eh_valida(lista, pos);
    printf("Qual o valor do novo item?\nValor desejado: ");
    scanf("%d", &novoItem->elem);
    
    if(lista->fst == NULL) {
        lista->fst = novoItem;
        lista->lst = novoItem;
        lista->tam = 1;

        return novoItem;
    } else if(pos == lista->tam) {
        lista->lst->prox = novoItem;
        lista->lst = novoItem;
        lista->tam += 1;

        return novoItem;
    }

    novoItem->prox = NULL;

    int i;
    for(i = 0, aux = lista->fst; aux != NULL; aux = aux->prox, ++i) {
        if(i == (pos - 1)) {
            novoItem->prox =  aux->prox;
            aux->prox = novoItem;
            lista->tam += 1;
            return novoItem;
        }
    }

}

void print_list(Lista *lista) {
    Celula *celula = lista->fst;
    printf("H->");
 
    while(celula) {
        printf("%d->", celula->elem);
        celula = celula->prox;
    }
 
    printf("|||\n");
}

void insere_em_lst(Lista *lista, int elemNovo) {
    Celula *novoItem = ALOCA(Celula, 1);
    
    novoItem->elem = elemNovo;
    novoItem->prox = NULL;
    
    if(lista->fst == NULL) {
        lista->fst = novoItem;
        lista->lst = novoItem;
        lista->tam = 1;
        
        return;
    }

    lista->lst->prox = novoItem;
    lista->tam += 1;
    lista->lst = novoItem;

    return;
}


Lista * copia_lista(Lista *listaAtual) {
    Lista *newList = inicializa_lista();

    Celula *cell = listaAtual->fst;
    Celula *newCell;

    while(cell) {
        insere_em_lst(newList, cell->elem);
        cell = cell->prox;
    }

    newList->tam = listaAtual->tam;
    newList->fst = listaAtual->fst;
    newList->lst = listaAtual->lst;

    return newList;
}

int has_showed(Lista *lista, int elemento) {
    Celula *item = lista->fst;
    int apareceu = 0;

    while(item) {
        if(item->elem == elemento) {
            apareceu = 1;
            return apareceu;
        }
    }

    return apareceu;
}

Lista * copia_ordenado(Lista *listaAtual) {
    Lista *newList = inicializa_lista();

    Celula *cell = listaAtual->fst;
    insere_em_lst(newList, cell->elem);

    int apareceu;
    
    while(cell){
        if(!(apareceu = has_showed(newList, cell->elem))) {
            insere_em_lst(newList, cell->elem);
            cell = cell->prox;
            continue;
        }
        cell = cell->prox;
    }

    return newList;
}

Lista * inverte_lista(Lista *listaOriginal){
    Lista *trashList = copia_lista(listaOriginal);
    Lista *inversa = inicializa_lista();
    
    inversa->fst = trashList->lst;
    inversa->fst = trashList->lst;

    // Celula *aux;
    Celula *cell = trashList->lst;
    Celula *newCell = inversa->fst;
    while(cell) {
        /*if(cell->prox == NULL) {
            newCell = cell;
            cell = NULL;
            cell = trashList->fst;
            newCell = newCell->prox;
        }*/
        newCell = cell;
        cell = NULL;
        newCell = newCell->prox;
    }
    free(trashList);
    return inversa;
}

/*int impl_cmp(int a, int b) {
    return (a - b);
}

void partition(Celula *lista, int pivot, Celula **l_ret, Celula **ge_ret) {
    Celula *l_tail = NULL;
    Celula *ge_tail = NULL;

    Celula *atual = lista;

    while (atual != NULL) {
        Celula *prox = atual->prox;

        atual->prox = NULL;
        if (impl_cmp(atual->elem, pivot) < 0) {
            if (l_tail == NULL) {
                 l_tail = atual;
                 *l_ret = l_tail;
            } else {
                l_tail->prox = atual;
                l_tail = atual;
            }
        } else {
            if (ge_tail == NULL) {
                 ge_tail = atual;
                 *ge_ret = ge_tail;
            } else {
                ge_tail->prox = atual;
                ge_tail = atual;
            }
        }
        atual = prox;
    }
}

Celula *concatena_3_listas(Celula *a, Celula *b, Celula *c) {
    Celula *head = a;
    Celula *tail = head;

    if (head != NULL) {
        head = tail = b;
    } else {
        while (tail->prox != NULL)  {
            tail = tail->prox;
        }
        tail->prox = b;
    }

    if (head != NULL) {
        head = tail = c;
    } else {
        while (tail->prox != NULL)  {
            tail = tail->prox;
        }
        tail->prox = c;
    }

    return head;
}

Celula *quicksort(Celula *lista) {
    //vou pegar como pivot sempre o primeiro da lista, removendo-o de lá; poderia usar outra técnica, mas essa é boa o suficiente para o exemplo
    Celula *pivot_lista = lista;
    int pivot = pivot_lista->elem;

    lista = lista->prox;
    pivot_lista->prox = NULL;

    Celula *menor, *maior;

    menor = maior = NULL;

    partition(lista, pivot, &menor, &maior);

    menor = quicksort(menor);
    maior = quicksort(maior);

    return concatena_3_listas(menor, pivot_lista, maior);
}*/

void insere_varios(Lista *lista) {
    int num, i = 1, elem;

    printf("Insira a quantidade de elementos que voce quer na lista: ");
    scanf("%d", &num);

    while(i <= num) {
        printf("Num para inserir: ");
        scanf("%d", &elem);
        insere_em_lst(lista, elem);
        ++i;
    }
}


int main(){
    Lista *newList, *inversa;
    Celula *newCel1, *newCel2, *newCel3, *newCel4;

    newList = inicializa_lista();
    insere_varios(newList);
    printf("Acabouuuuuuuuuuuuuuuuuuuuuuuuuuuuuu de inseriiiiiiiiiiiiiiiiiir\n");
    // newCel1 = insere_em_lst(newList);
    // newCel2 = insere_em_lst(newList);
    // newCel3 = insere_em_lst(newList);
    // newCel4 = insere_em_lst(newList);

    print_list(newList);
    // Celula *ordenado = quicksort(newList->fst);
    // print_list(ordenado);
    // inversa = inverte_lista(newList);
    // print_list(inversa->fst);
    Lista *oi = copia_lista(newList);
    print_list(oi);
    Lista *cop = copia_ordenado(newList);
    print_list(cop);
    

    // free(inversa);
    free(newList);
    free(oi);
    // free(newCel1);
    // free(newCel2);
    // free(newCel3);
    // free(newCel4);


    //Celula *ordenado = quicksort(newList->fst);

    return 0;
}