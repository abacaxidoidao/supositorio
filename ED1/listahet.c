#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "listahet.h"

struct cliente {
    char *nome;
    int id;
};

struct movel {
    int placa;
    int ano;
    float valor;
};

struct imovel {
    int identificador;
    int ano;
    float valor;
};

struct listahet {
    Cliente *dono;
    void *item;
    ListaHet *nxt;
    int id;
};


ListaHet * cria() {
    ListaHet *newList = (ListaHet *)malloc(sizeof(ListaHet));

    newList->id = -1;
    newList->item = NULL;
    newList->nxt = NULL;
    newList->dono = NULL;

    return newList;
}

int lista_ehvazia(ListaHet *lista) {
  return(lista->item == NULL);
}

Cliente * cria_cliente(char *nome, int id) {
    Cliente *newCliente = (Cliente *)malloc(sizeof(Cliente));

    newCliente->nome = (char *)malloc((strlen(nome) + 1)* sizeof(char));
    strcpy(newCliente->nome, nome);

    newCliente->id = id;

    return newCliente;
}

Movel * cria_movel(int placa, int ano, float valor) {
    Movel *newMovel = (Movel *)malloc(sizeof(Movel));

    newMovel->ano = ano;
    newMovel->placa = placa;
    newMovel->valor = valor;

    return newMovel;
}

Imovel * cria_imovel(int id, int ano, float valor) {
    Imovel *newImovel = (Imovel *)malloc(sizeof(Imovel));

    newImovel->ano = ano;
    newImovel->identificador = id;
    newImovel->valor = valor;

    return newImovel;
}

ListaHet * insere_movel(ListaHet *lista, Cliente *dono, Movel *item) {
    ListaHet *novo = cria();
    if(lista_ehvazia(lista)) {
        lista = novo;
        novo->nxt = NULL;
        novo->dono = dono;
        novo->item = (Movel *)item;
        novo->id = 0;

        return lista;
    }

    novo->nxt = lista;
    lista = novo;
    novo->dono = dono;
    novo->item = (Movel *)item;
    novo->id = 0;

    return lista;
}

ListaHet * insere_imovel(ListaHet *lista, Cliente *dono, Imovel *item) {
    ListaHet *novo = cria();
    if(lista_ehvazia(lista)) {
        lista = novo;
        novo->nxt = NULL;
        novo->dono = dono;
        novo->item = (Imovel *)item;
        novo->id = 1;

        return lista;
    }

    novo->nxt = lista;
    lista = novo;
    novo->dono = dono;
    novo->item = (Imovel *)item;
    novo->id = 1;

    return lista;
}

void imprime(ListaHet *lista) {
    if(lista_ehvazia(lista)) {
        fprintf(stderr, "ERRO: Lista eh vazia\n");
        return;
    }

    ListaHet *p = lista;
    while(p) {
        if(p->id){
            printf("ID: %d\n", ((Imovel*)p->item)->identificador);
            printf("Dados do imovel:\nAno: %d, valor: %f\n", ((Imovel*)p->item)->ano, ((Imovel*)p->item)->valor);
            printf("Dados do dono:\nNome: %s, ID: %d\n", p->dono->nome, p->dono->id);
        }else{
            printf("Placa: %d\n", ((Movel*)p->item)->placa);
            printf("Dados do movel:\nAno: %d, valor: %f\n", ((Movel*)p->item)->ano, ((Movel*)p->item)->valor);
            printf("Dados do dono:\nNome: %s, ID: %d\n", p->dono->nome, p->dono->id);
        }
        p = p->nxt;
    }
}

ListaHet * procura_id(ListaHet *lista, int id_cliente) {
    if(lista_ehvazia(lista)) {
        fprintf(stderr, "ERRO: Lista eh vazia\n");
        return lista;
    }

    ListaHet *p = lista;

    while(p) {
        if(id_cliente == p->dono->id)
            return p;
        p = p->nxt;
    }
    return NULL;
}

void destroi_celula(ListaHet *celula) {
  if(lista_ehvazia(celula)) {
    fprintf(stderr, "ERRO: Lista eh vazia\n");
  }
  free(celula->dono->nome);
  free(celula->dono);
  free(celula->item);
  free(celula);
}

ListaHet * retira_cliente(ListaHet *lista, int id_cliente) {
    if(lista_ehvazia(lista)) {
        fprintf(stderr, "ERRO: Lista eh vazia\n");
        return lista;
    }

    ListaHet *n_l = cria();
    ListaHet *p = lista;
    ListaHet *nxt = NULL;

    while (p) {
      nxt = p->nxt;

      if(p->dono->id == id_cliente) {
        // destroi_celula(p);
        printf("DESTRUIU\n");
      }
      else {
        if(p->id) {
          n_l = insere_imovel(n_l, p->dono, p->item);
          printf("IMOVEL\n");
        }
        else
          n_l = insere_movel(n_l, p->dono, p->item);
        printf("MOVEL\n");
      }
        p = nxt;
    }

    p = lista;

    while (p) {
      nxt = p->nxt;
      destroi_celula(p);
      p = nxt;
    }

    return n_l;
}


float calcula_valor_assegurado(ListaHet *lista, Cliente *dono, float taxa_movel, float taxa_imovel) {
    if(lista_ehvazia(lista)) {
        fprintf(stderr, "ERRO: Lista eh vazia\n");
        return -1.0;
    }

    ListaHet *p;
    float taxa;

    for(p = lista; p; p = p->nxt) {
        if(p->dono->id == dono->id) {
            if(p->id) {
                taxa = ((Imovel*)p->item)->valor * taxa_imovel;
            } else {
                taxa = ((Movel*)p->item)->valor * taxa_movel;
            }
        }
    }
    return taxa;
}
