#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilha.h"

struct pessoa {
  char *nome;
  int idade;
  char *endereco;
};

struct pilha {
  Pessoa *pessoas[10];
  int topo;
};

Pilha * cria_pilha() {
  Pilha *new_p = (Pilha *)malloc(sizeof(Pilha));

  new_p->topo = 0;

  return new_p;
}

int vazia_pilha(Pilha *pilha) {
  return (pilha->topo == 0);
}

int tamanho_pilha(Pilha *pilha) {
  return pilha->topo;
}

Pessoa* inicializaPessoa(char* nome, int idade, char* endereco) {
  Pessoa *p = (Pessoa *)malloc(sizeof(Pessoa));

  p->nome = (char *)malloc(sizeof(char) * (strlen(nome) + 1));
  strcpy(p->nome, nome);

  p->endereco = (char *)malloc(sizeof(char) * (strlen(endereco) + 1));
  strcpy(p->endereco, endereco);

  p->idade = idade;

  return p;
}

void push(Pessoa *pessoa, Pilha *pilha) {
  if(pilha->topo < 10) {
    pilha->pessoas[pilha->topo] = pessoa;
    (pilha->topo)++;
  }
  else
    printf("CHEIO\n");

  return;
}

Pessoa *pop(Pilha *pilha) {
  Pessoa *p;
  if(pilha->topo == 0)
    return NULL;

  else {
    p = pilha->pessoas[pilha->topo - 1];
    (pilha->topo)--;
  }
  return p;
}

void destroi_pessoa(Pessoa *pessoa) {
  if(pessoa) {
    free(pessoa->nome);
    free(pessoa->endereco);
    free(pessoa);
  }
}

void imprimepessoa(Pessoa *p) {
  if (p) {
    printf("Idade: %d\n", p->idade);
    printf("Nome: %s\n", p->nome);
    printf("Endereco: %s\n", p->endereco);
  }
}

void imprime_pilha(Pilha *pilha) {
  for (int i = 0; i < pilha->topo; i++) {
    imprimepessoa(pilha->pessoas[i]);
  }
}

int retorna_idade(Pessoa *p) {
  return p->idade;
}

Pilha *destroi_pilha(Pilha *pilha) {
  Pessoa *popped;

  while (!vazia_pilha(pilha)) {
    popped = pop(pilha);
    destroi_pessoa(popped);
    // pilha->topo--
  }
  free(pilha);

  return NULL;
}
