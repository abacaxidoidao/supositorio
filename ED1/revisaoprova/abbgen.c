#include <stdio.h>
#include <stdlib.h>
#include "abbgen.h"

struct arv {
  void *info;
  Arv_t *esq, *dir;
};

Arv_t * cria_vazia(void) {
  return NULL;
}

Arv_t * insere(Arv_t *a, int (*cmp)(void *, void *), void *info) {
  if (!a) {
    a = (Arv_t *)malloc(sizeof(Arv_t));
    a->info = info;
    a->esq = a->dir = NULL;
  }
  else if (cmp(a->info, info)) 
    a->esq = insere(a->esq, cmp, info);
  else 
    a->dir = insere(a->dir, cmp, info);

  return a;
}

void percorre_arv(Arv_t *t, void (*cb)(void *)) {
  if (!t)
    return;

  if (t->esq) 
    percorre_arv(t->esq, cb);
  if (t->dir) {
    percorre_arv(t->dir, cb);
    if (cb) 
      cb(t->info);
  }
}

void * search_elem(Arv_t *t, int (*cmp)(void *, void *), void *elem) {
  if (!t)
    return NULL;
  if (cmp(t->info, elem))
    return t->info;

  if (t->esq) 
    return search_elem(t->esq, cmp, elem);
  if (t->dir)
    return search_elem(t->dir, cmp, elem);

  return NULL;
}

void *info(Arv_t *t) {
  return t->info;
}

// void remove(Arv_t *t, int (*cmp)(void *, void *), void *elem) {

// }