#include "abbgen.h"

#ifndef AL_H_
#define AL_H_

typedef struct bl Bl_t;

Bl_t * init_est(int, char);

int compara(void *, void *);

void dest(Bl_t *);

Bl_t * s_a(Arv_t *, int);

#endif //AL_H_