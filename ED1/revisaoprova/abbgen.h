#ifndef ARV_H_
#define ARV_H_

typedef struct arv Arv_t;

void * search_elem(Arv_t *, int (*cmp)(void *, void *), void *);

void *info(Arv_t *);

void percorre_arv(Arv_t *, void (*cb)(void *));

Arv_t * cria_vazia(void);

Arv_t * insere(Arv_t *, int (*cmp)(void *, void *), void *);

#endif //ARV_H_