#include <stdio.h>
#include <stdlib.h>
#include "abbgen.h"
#include "estrutura.h"

struct bl {
  int mat;
  char nome;
};

Bl_t * init_est(int mat, char c) {
  Bl_t *a = (Bl_t *)malloc(sizeof(Bl_t));

  a->mat = mat;
  a->nome = c;

  return a;
}

int compara(void *a, void *mat) {
  Bl_t *a_ = a;
  int *mat_ = mat;
  
  return a_->mat == *mat_;
}

Bl_t * s_a(Arv_t *t, int mat) {
  return search_elem(t, compara, &mat);
}

void dest(Bl_t *a) {
  free(a);
}