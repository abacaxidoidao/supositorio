#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#define TAM 127
#define NP 64

struct word {
  char word[NP];
  int n;
  struct word *prox;
};

typedef struct word Word;
typedef Word* Hash[TAM];

void init(Hash tab) {
  for (int i = 0; i < TAM; i++)
    tab[i] = NULL;
}

int le_pal(FILE *file, char *s) {
  int c, i = 0;
  while ((c = fgetc(file)) != EOF) {
    if (isalpha(c))
      break;
  }
  if (c == EOF)
    return 0;

  while (i < NP - 1 && (c = fgetc(file)) != EOF && isalpha(c))
    s[i++] = c;
  s[i] = '\0';

  return 1;
}

int hash_f(char *s) {
  int pos = 0;
  for (int i = 0; i != '\0'; i++) 
    pos += (int)s[i];

  return pos%TAM;
}

Word * acessa(Hash tab, char *pal) {
  int pos = hash_f(pal);
  Word *p;

  for (p = tab[pos]; p != NULL; p = p->prox)
    if (!strcmp(p->word, pal)) {
      printf("%s\n", pal);
      return p;
    }
  
  p = (Word *)malloc(sizeof(Word));
  strcpy(p->word, pal);
  p->n = 0;
  p->prox = tab[pos];
  tab[pos] = p;

  printf("%s\n", pal);
  return p;
}

int conta(Hash tab) {
  int t = 0;
  Word *p;

  for (int i = 0; i < TAM; i++) 
    for (p = tab[i]; p != NULL; p = p->prox) {
      // printf("%s\n", p->word);
      t++;
    }
  // printf("%d\n", t);
  return t;
}

Word ** init_vet(Hash tab) {
  int j = 0;
  Word *p;

  Word ** vet = (Word **)calloc(conta(tab), sizeof(Word));

  for (int i = 0; i < TAM; i++)
    for (p = tab[i]; p != NULL; p = p->prox) {
      // printf("%s\n", p->word);
      vet[j++] = p;
    }

  return vet;
}

int comp(const void *q1_,const void *q2_) {
  Word *q1 = (Word *)q1_;
  Word *q2 = (Word *)q2_;

  if ((q1)->n > (q2)->n) return 0;
  return 1;
}

void imprime(Hash tab) {
  Word **vet = init_vet(tab);

  int i, j = conta(tab);

  for (i = 0; i < j; i++)
    printf("%s -> %d\n", vet[i]->word, vet[i]->n);

  qsort(vet, j, sizeof(Word*), comp);
  printf("\n\n");
  for (i = 0; i < j; i++)
    printf("%s -> %d\n", vet[i]->word, vet[i]->n);
}


int main(int argc, char const *argv[]){
  if (argc != 2) {
    printf("nao\n");
    exit(1);
  }
  FILE *f = fopen(argv[1], "r");
  if (!f) {
    printf("ero\n");
    exit(1);
  }
  char s[NP];
  Hash t;
  init(t);
  while (le_pal(f, s)) {
    Word *p = acessa(t, s);
    p->n++;
  }
  imprime(t); 
  
  // FILE *fp =  


  return 0;
}

