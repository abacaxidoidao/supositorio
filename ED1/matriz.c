#include <stdio.h>
#include <stdlib.h>
#include "matriz.h"

#define ALOCA(TYPE, SIZE) ((TYPE*)malloc((SIZE) * sizeof(TYPE)))

struct matriz {
    int **matriz;
    int nlinhas;
    int ncolunas;
};

Matriz * inicializaMatriz(int nlinhas, int ncolunas) {
    if(nlinhas == 0 && ncolunas == 0) {
        printf("Numero de linhas e colunas nao compativeis\n");
        exit(1);
    }

    int i;

    Matriz *mat = ALOCA(Matriz, 1);
    mat->matriz = ALOCA(int*, nlinhas);

    for(i=0;i<nlinhas;i++){
        mat->matriz[i] = ALOCA(int, ncolunas);
    }
    mat->nlinhas = nlinhas;
    mat->ncolunas = ncolunas;

    return mat;
}


void checa_se_null(Matriz *matriz) {
    if(matriz == NULL) {
        printf("Matriz nao existe!\n");
        exit(1);
    }

}

void linhaecol_disp(Matriz *mat, int linha, int coluna) {
    if((linha < 0) || (linha > mat->nlinhas) || (coluna < 0) || (coluna > mat->ncolunas)) {
        printf("Linha ou coluna que deseja acessar nao existe\n");
        exit(1);
    }
}

void cmpr_colelinha(int coluna, int linha) {
    if(coluna != linha) {
        printf("Coluna da matriz 1 e linha da matriz 2 nao compativeis\n");
        exit(1);
    }
}

void modificaElemento(Matriz *mat, int linha, int coluna, int elem) {
    checa_se_null(mat);
    linhaecol_disp(mat, linha, coluna);

    mat->matriz[linha][coluna] = elem;
}

int recuperaElemento(Matriz *mat, int linha, int coluna) {
    checa_se_null(mat);
    linhaecol_disp(mat, linha, coluna);

    return mat->matriz[linha][coluna];
}

int recuperaNColunas(Matriz *mat) {
    checa_se_null(mat);

    return mat->ncolunas;
}

int recuperaNLinhas(Matriz *mat) {
    checa_se_null(mat);

    return mat->nlinhas;
}

Matriz * transposta(Matriz *mat) {
    checa_se_null(mat);

    int i, j;

    Matriz *transpose = ALOCA(Matriz, 1);
    transpose->matriz = ALOCA(int *, mat->ncolunas);
    for(i=0;i<mat->ncolunas;i++) {
        transpose->matriz[i] = ALOCA(int, mat->nlinhas);
    }

    for(i=0;i<mat->nlinhas;i++){
        for(j=0;j<mat->ncolunas;j++){
            transpose->matriz[j][i] = mat->matriz[i][j];
        }
    }
    transpose->nlinhas = mat->ncolunas;
    transpose->ncolunas = mat->nlinhas;

    return transpose;
}

Matriz * multiplicacao(Matriz *mat1, Matriz *mat2) {
    checa_se_null(mat1);
    checa_se_null(mat2);
    cmpr_colelinha(mat2->ncolunas, mat1->nlinhas);

    int i, j, k, nli = recuperaNLinhas(mat1), ncol = recuperaNColunas(mat2);
    
    Matriz *matMult = ALOCA(Matriz, 1);
    matMult->matriz = ALOCA(int *, mat1->nlinhas);
    for(i=0;i<mat1->nlinhas;i++){
        matMult->matriz[i] = ALOCA(int, mat2->ncolunas);
    }

    matMult->nlinhas = nli;
    matMult->ncolunas = ncol;

    for(i=0;i<nli;i++){
        for(j=0;j<ncol;j++){
            matMult->matriz[i][j] = 0;
            //usar modifica elemento
            for(k=0;k<mat2->nlinhas;k++){
                matMult->matriz[i][j] = mat1->matriz[i][k] * mat2->matriz[k][j];
            }
        }
    }

    return matMult;
}

void imprimeMatriz(Matriz *mat) {
    checa_se_null(mat);

    int i, j;

    for(i=0;i<mat->nlinhas;i++){
        for(j=0;j<mat->ncolunas;j++){
            printf("%3d ", mat->matriz[i][j]);
        }
        printf("\n");
    }
}

void destroiMatriz(Matriz *mat) {
    checa_se_null(mat);

    int i;

    for(i=0;i<mat->nlinhas;i++){
        free(mat->matriz[i]);
    }
    free(mat->matriz);
    free(mat);
}