#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char * initVec(char *keyword) { //inicializa um vetor
    size_t tam = strlen(keyword); //a partir da palavra chave
    int i;                        //onde tds os caracteres serao '_'
    char *vetor;

    vetor = (char *)malloc(sizeof(char) * tam);
    for(i = 0; i < tam; ++i)
        vetor[i] = '_';

    return vetor;
}

int substitui(char* keyword, char* vetor, char letra) {//Auto-explicativo
    int i, tam = strlen(keyword);

    int flag = 0;
    for(i = 0; i < tam; i++) {
        if(keyword[i] == letra) {
            vetor[i] = letra;
            flag = 1;
        }
    }

    return flag;
}

int ehElem(char c, char *string) { //Checa se um char c esta contido num char *string
    size_t tam = strlen(string);   //se for, retorna sua posicao
    int i;
    
    for(i = 0; i < tam; ++i) 
        if(string[i] == c)
            return i;

    return -1;
}

int vrfyWord(char *keyword, char *vetor) { //verifica se a palavra ainda contem '_'
    size_t tam = strlen(keyword);
    int i;

    for(i = 0; i < tam; ++i)
        if(ehElem('_', vetor) >= 0)
            return 1;
    
    return 0;

}

void printVet(char *vetor) {
    size_t tam = strlen(vetor);
    int i;

    for(i = 0; i < tam; ++i)
        printf("%c ", vetor[i]);
    
    printf("\n");
}

int main() {
    char *keyword, dig;
    int c, i = 0, errors = 0;

    keyword = (char *)malloc(sizeof(char) * 1);
    printf("Digite a palavra-chave: ");
    while((c = getchar()) != 10) {
        keyword[i] = c;
        i++;
        keyword = realloc(keyword, sizeof(char) * i);
    }
    system("clear");

    char *vetor = initVec(keyword);

    while(vrfyWord(keyword, vetor) && errors != 5) {
        printf("Digite uma letra: ");
        scanf(" %c", &dig);
        
        int subs = substitui(keyword, vetor, dig);
        
        if(!subs)
            errors++;
        
        printVet(vetor);
        printf("Erros: %d / 5\n", errors);
    }

    return 1;
}