#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int ** malloc_vec(int m, int n) {
    int **Vec, i;

    Vec = (int **)malloc(m * sizeof(int *));

    for(i = 0; i < m; i++) 
        Vec[i] = (int *)malloc(n * sizeof(int));
    

    return Vec;
}

void le_valores(int **Vec, int m, int n) {
    int i, j;
    
    for(i = 0; i < m; i++) {
        for(j = 0; j < n; j++) {
            printf("Insira o valor que ocupará a posição (%d/%d): ", i, j);
            scanf("%d", &Vec[i][j]);
        }
    }
}

void printf_vet(int **Vec, int m, int n) {
    int i, j;

    for(i = 0; i < m; i++) {
        for(j = 0; j < n; j++) {
            printf("%3d ", Vec[i][j]);
        }
        printf("\n");
    }
}

void free_vet(int **Vec, int m) {
    int i;

    for(i = 0; i < m; i++) {
        free(Vec[i]);
    }
    free(Vec);
}

void mult_mat(int **mat1, int **mat2, int m, int n, int p) {
    int i, j, k, **VetRes;

    VetRes = malloc_vec(m, n);

    for(i = 0; i < (m ); i++) {
        for(j = 0; j < (n ); j++) {
            VetRes[i][j] = 0;
            for(k = 0; k < (p ); k++) {
                VetRes[i][j] = VetRes[i][j] + (mat1[i][k] * mat2[k][j]);
            }
        }
    }

    printf_vet(VetRes, m, n);
    free_vet(VetRes, m);
}

int main() {
    int **mat1, **mat2, m1, n1, m2, n2;

    do {
        printf("O numero de colunas da matriz 1 deve ser igual ao numero de linhas da matriz 2!");
        printf("\nDefina os tamanhos das matrizes a serem multiplicadas.\nMatriz 1: ");
        scanf("%d %d", &m1, &n1);
        printf("Matriz 2: ");
        scanf("%d %d", &m2, &n2);
    } while(n1 != m2);

    mat1 = malloc_vec(m1, n1);
    mat2 = malloc_vec(m2, n2);

    printf("Acesse as posicoes da primeira matriz: \n");
    le_valores(mat1, m1, n2);
    printf("Acesse as posicoes da segunda matriz: \n");
    le_valores(mat2, m2, n2);

    mult_mat(mat1, mat2, m1, n2, n1);
    free_vet(mat1, m1);
    free_vet(mat2, m2);
    
    return 0;
}