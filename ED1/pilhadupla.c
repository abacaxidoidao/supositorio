#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pilhadupla.h"

#define MAXTAM 10

struct indice {
  int base, topo;
}

struct item {
  char *nome;
};

struct pilhadupla {
  Item item[MAXTAM];
  IndiceP p1, p2;
};

PilhaDupla * init_pilha(void) {
  Pilha *p = (Pilha *)malloc(sizeof(Pilha));

  p->p1.topo = p->p1.base = 0;

  p->p2.topo = p->p2.base = MAXTAM - 1;

  return p;
}

Item * init_item(char *str) {
  Item * i = (Item *)malloc(sizeof(Item));

  i->nome = (char *)malloc(sizeof(char) * (strlen(str) + 1));
  strcpy(i->nome, str);

  return i;
}

int pilha_vazia(Pilha *p, int i) {
  if (i == 1) {
    return (p->p1.topo == 0);
  } else if (i == 2) {
    return (p->p2.topo == MAXTAM - 1);
  } else
    return -1;
}

void empilha_dupla(Pilha *p, char *str, int id) {
  if(p->p1.topo == p->p2.topo - 1) {
    printf("TA CHEIO\n");
  } else {
    Item *i = init_item(str);
    if(id == 2) {
      p->item[p->p2.topo] = i;
      p->p2.topo--;
    } else if (id == 1) {
      p->item[p->p1.topo] = i;
      p->p1.topo++;
    } else {
      printf("TA DOIDO?\n");
      return;
    }



  }

}
