#include <stdio.h>

void printf_vet(size_t tam, int qtdElem, void *vetor, char *type) {
    int i,vetr[qtdElem];

    for(i=0;i<qtdElem;i++){
        vetr[i]=(int)vetor[i];
    }

    for(i=0;i<qtdElem;i++)
        printf(type,vetr[i]);
    printf("\n");

}

int main(){
    int vet[] = { '1', '2', '3', '4', '5'};
    printf_vet(sizeof(int), 5, vet, "%d");
    return 0;
}