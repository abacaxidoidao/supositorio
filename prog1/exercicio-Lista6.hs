{-a) Dadas duas strings xs e ys, verificar se xs é prefixo de ys.-}

qndP xs = length xs

pert xs ys = take (qndP xs) ys

ehPref xs ys = xs == pert xs ys

{-b) Dadas duas strings xs e ys, verificar se xs é sufixo de ys.-}

qndD xs ys = length ys - length xs 

pert2 xs ys = drop (qndD xs ys) ys

ehSuf xs ys = xs == pert2 xs ys

{-c) Dadas duas strings xs e ys, verificar se xs é sublista de ys.-}

subLis xs ys = not $ null [True | i <- [0..length ys-length xs], (take (length xs) (drop i ys) == xs)]
 


{-d) Verificar se uma string é um palíndrome (isso acontece quando a string é a mesma quando lida da
esquerda para a direita ou da direita para a esquerda).-} 

ehPalin xs = [ xs!!i | i <- [length xs - 1, length xs-1-1 .. 0]] == xs




ehPalin' xs = xs == reverse xs


 
{-e) Verificar se os elementos de uma lista são distintos.-}

boolToInt x = if x then 1 else 0

distElem xs = length [x | x <- xs, (length $ (filter (==True) $ map (==x) xs)) == 1] == length xs



{-f) Dada uma lista de elementos, obter a lista dos mesmos 
elementos
, porém com
 a ordem inversa.-}

reverse' xs = [ xs!!i | i <- [length xs - 1, length xs-1-1 .. 0]]

{-g) Determinar a posição (ou posições) de um elemento 
x em uma lista xs, se ele ocorre na lista.-}

posicoes xs c = [ i | i <- [0..length xs - 1], xs!!i==c ]

{-
h) Dada uma lista de números ordenada, e um valor, inserir esse valor na lista, de maneira que ela 
pernaneça ordenada. Exemplo: insereOrdenada 8 [-9, -1,2,6,7,14,15,20] => [-9, -1,2,6,7,8,14,15,20]
-}

ehCres xs = length xs - 1 == length ([True | i <- [0..length xs-2], xs!!i<xs!!(i+1)])

ehDecr xs = length xs - 1 == length ([True | i <- [0..length xs-2], xs!!i>xs!!(i+1)])

insereElem xs x
      | ehCres xs = fst a ++ [x] ++ snd a
      | ehDecr xs = fst b ++ [x] ++ snd b
           where
               a = break (>x) xs 
               b = break (<x) xs


{-
i) Dada uma lista de duplas formadas pelos nomes de pessoas e suas respectivas idades, faça uma 
função que informe o(s) nome(s) da(s) pessoa(s) mais nova(s) desta lista. A resposta deve ser então 
uma lista de nomes.
-}

listNo xs = [ x | (x,y) <- xs]
listId xs = [ y | (x,y) <- xs]

nomeId xs = [ x | (x,y) <- xs, null $ (filter (<y) (listId xs))]

--teste xs = [ x | x <- xs, null $ filter (<x) xs] 













