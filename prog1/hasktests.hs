import qualified Data.Map as M


-- circ r = 2 * pi * r
-- lucky x | x == 1    =    "LUCKY NUMBER ONE!"
--         | x == 2    =    "LUCKY NUMBER TWO!"
--         | x == 3    =    "LUCKY NUMBER THREE!"
--         | otherwise =    "NT"

-- fat x | x > 1     = x * fat (pred x)
--       | otherwise = 1

-- fat x = product [x]


-- test [] = 0
-- test (y:x) = y + test x

-- teste xs@(x:y) = "Teste" ++ xs ++ [x]

-- Pega valores de altura e peso e retorna listas de IMC
--
-- calcBmis xs = [bmi w h | (w, h) <- xs]
--     where bmi weight height = weight / height ^ 2



-- Leve modificacão na funcão
--
-- calcBmis xs = [if bmi w h < 30 then 0
--                 else 1| (w, h) <- xs]
--     where bmi weight height = weight / height ^ 2
--


-- Calcula área de um cilindro
-- aC r l = let aL   = 2 * pi * r * l
--              aCI  = pi * r^2
--          in  aL + 2 * aCI


-- Calcula a área de um cilindro de modo diferente
-- aC r l = 2 * aCI + aL
--         where
--           aCI = pi * r^2
--           aL  = 2 * pi * r * l


-- teste x n | n == 0    = []
--           | otherwise = n:teste x (n-1)
-- replica x = teste x 4
-- replico = teste 4
--
-- fals (x:xs) | x == 0    = []
--             | otherwise = fals xs ++ [x]
--


-- tst x = tst (tst x)
-- applyTwice f x = f (f x)

-- teste :: (t, t1) -> (t, t1)

-- teste a = a

-- func x y = [x | x <- [1..], x < y]

-- c = (++3)



-- c :: Num a => a -> a


-- tripi = [[a, b, c] | a <- [1..40], b <- [1..40], c <- [1..40], a^2 == b^2 + c^2, b > c]

-- teste = [x | x <- [100000, 99999..0], x > 90000, mod x 3829 == 0 ]

{-
teste = head (filter a [100000, 99999..1])
        where
         a x = mod x 3829 == 0
-}

-- teste :: [a] -> Bool

{-

IMPORTANTE: (== mod 3)   = (Eq (a -> a), Integral a) => (a -> a) -> Bool
            mod (3 ==)   = (Eq a, Integral (a -> Bool), Num a) => (a -> Bool) -> a -> Bool
            (mod 3 3 ==) = Integral a => a -> Bool

-}

{-
teste = filter f [1..10]
        where
           f y = mod y 4 == 0
-}
{-
teste x = f x (+ 3)
       where
           f x = mod x 3 == 0
-}

{-
teste x = f x
      where
          f x = mod x 3 == 0
-}

{-

teste x | x == 1    = ("Teste", 10, [1, 3])
        | x == 2    = ("Tuts", 9, [2, 4])
        | otherwise = ("foo", 0, [])

cur x y | n1 > n2   = n1
        | otherwise = n2
    where
       (s1, n1, _) = teste x
       (s2, n2, _) = teste y

O "_" serve para não especificar o tipo de parâmetro que a funcão recebe ou retorna. Mais viável quando se trata de generalizar a saída, como no caso da funcão acima

-}


{-
teste a b c = (2*a, 2*b, 2*c)

cur x y a b = if b1 > b2
               then b1
               else b2
            where
                (_, b1, _) = teste a x b
                (_, b2, _) = teste a y b

-}

-- Questao da distancia de Hamming resolvida de tres diferentes formas

{-

bool x = if x then 0 else 1

distHamming n1 n2
      | g==0 || g2==0     = "Os dois numeros precisam ter 5 algarismos"
      | g>=10 || g2 >= 10 = "Os dois numeros precisam ter 5 algarismos"
      | otherwise = show(bool (g==g2) + bool (h==h2) + bool (f==f2) + bool (d==d2) + bool(b==b2))
                  where
                        (a,b) = divMod n1 10
                        (c,d) = divMod a 10
                        (e,f) = divMod c 10
                        (g,h) = divMod e 10
                        (a2,b2) = divMod n2 10
                        (c2,d2) = divMod a2 10
                        (e2,f2) = divMod c2 10
                        (g2,h2) = divMod e2 10


f 0 = []
f x = mod x 10 : f(div x 10)
lista n1 = reverse . f $ n1

difAlgs n1 n2 = zipWith (-) (lista n1) (lista n2)

distHamming2 n1 n2 = length . filter (/=0) . difAlgs n1 $ n2

distHamming' = curry $ length . filter (/=0) . (uncurry difAlgs)

{-

-- jesus amado separe esses scripts

teste = 5 + ab
      where
            ab = 3


soma a b = a + b

teste2 x = soma 5 ((\x -> x*2) x)

teste3 x = 5 + ((\x -> x*2) x)
-}


triPit a b c = [ (a,b,c) | a <- [1..a], b <- [1..b], c <- [1..c], a^2 == b^2 + c^2, a > b, b > c]
-}


{-
testeATC :: (t, t1) -> (t, t1) -> (t, t1) -> [(t, t1)]
testeATC a b c = [a, b, c]
-}

-- list :: (t, t1) -> (t, t1) -> (t, t1) -> [(t, t1)] -> [(Char)]


tdsP l = [(x,y) | x <- l, y <- l]

list a b c l = elem (a1,a1) l && elem (b1,b1) l && elem (c1,c1) l
            where
                  (a1,_) = a
                  (b1,_) = b
                  (c1,_) = c


posicoes xs c = [ i | i <- [0..length xs - 1], xs!!i==c ]


semElemDupli xs = [ x | x <- xs, length (posicoes xs x) == 1]

pP a b c = [(x,y) | x <- [a,b,c], y <- [a,b,c]]

ehRef 0 0 0 = True
ehRef a b c = (a,a) `elem` li && elem (b,b) li && elem (c,c) li
        where
          li = pP a b c

ehSim 0 0 0 = True
ehSim a b c = elem (a,b) li && elem (b,a) li && elem (a,c) li && elem (c,a) li && elem (c,b) li && elem (b,c) li
        where
          li = pP a b c
