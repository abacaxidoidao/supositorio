#!/usr/bin/python

#string1 = "ababa"

igual = "="



def inverter(string):
    resultado = ""

    n = 0
    while n < len(string):
        resultado = resultado + string[len(string) - 1 - n]
        n = n + 1

    return resultado



def comparar(string1, string2):
    if len(string1) != len(string2):
        return False
    else:
        n = 0
        while n < len(string1):
            #print n, string1[n], string2[n]

            if string1[n] != string2[n]:
                return False

            n=n+1

        return True


def eh_invertivel(string):
    return comparar(string, inverter(string))



# == teste ==


print eh_invertivel("12321")


# print (inverter('kelvin'))

# print comparar('kelvin', 'kelv'), '\n'


# print comparar('kelvi', 'kelvin'), '\n'

# print comparar('kelvin', 'kelvon'), '\n'

# print comparar('kelvin', 'kelvim'), '\n'

# print comparar('kelvin', 'kelvin'), '\n'
