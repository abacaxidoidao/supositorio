insere k [] = [k] -- insere um elemento em uma lista ordenada
insere k (x:xs) = if x < k then x : insere k xs else k : x : xs

ord :: [String] -> [String]
ord [] = []
ord (x:xs) = insere x (ord xs)

--______________________________________________________________--

ehPref _ [] = False -- checa se é prefixo
ehPref [] _ = True
ehPref xs ys = (head xs == head ys && ehPref (tail xs) (tail ys)) || xs == ys

 


subs _ [] _ = []
--subs _ ys [] = ys
--subs [] ys _ = ys
subs xs (y:ys) zs = if not (ehPref xs (y:ys))
                 then y : subs xs (ys) zs
                 else zs ++ " " ++ subs xs (drop xss ys) zs--(zs ++ ys)
  where
    xss = length xs

--______________________________________________________________--


intercala xs [] = xs
intercala [] ys = ys
intercala (x:xs) (y:ys) = (min x y) : (max x y) : intercala xs ys

--______________________________________________________________--


--ord' :: Ord a => (a -> b) -> [a] -> [a]
mediasAlunos :: Ord a => [(String,a)] -> [(String,a)]
mediasAlunos [] = []
mediasAlunos (x:xs) = insereOn x (mediasAlunos xs)

insereOn k [] = [k] -- parece o insere mas funciona para lista de tuplas
insereOn k (x:xs) = if snd x < snd k then x : insereOn k xs else k : x : xs

--______________________________________________________________--

ordenaL [] = []
ordenaL (x:xs) = insereTam x (ordenaL xs)

insereTam xs [] = [xs] -- parece o insere mas funciona para lista de listas
insereTam xs (y:ys) = if length y < length xs
                       then y : insereTam xs ys
                       else xs : y : ys

--______________________________________________________________--

superOrd [] = []
superOrd (x:xs) = if not $ null $ listaMenores
                   then superOrd (xs++[x])
                   else x : superOrd xs
  where
   listaMenores = filter (<x) xs

--______________________________________________________________--


sumsq n = foldr (+) 0 [x^2 | x <- [n,n-1..0]]


--______________________________________________________________--

fat 0 = 1
fat x = x * fat (x-1)

somatorio n = foldl (+) 0 [1/(fat x) | x <- [0..n]]


--______________________________________________________________--


tamanhoMat mat 
     | matQuad mat = truncate $ sqrt (fromIntegral $ length mat)
     | otherwise   = error "Matriz não é quadrada"

matQuad [] = error "Lista vazia"
matQuad mat = (length . show $ sqrt (fromIntegral $ length mat)) <= 4


transf [] _ = [] ++ []
transf mat matz = [take x mat] ++ transf (drop x mat) matz
  where
    x = tamanhoMat matz


--transfMat _ [] = [] ++ []
--transfMat mat matz  = [take x matz] ++ transfMat mat (drop x matz) 
--     | otherwise = error "Matriz não é quadrada"
--  where
--    x = tamanhoMat mat

transfMat mat 
     | matQuad mat = transf mat mat
     | otherwise   = error "Matriz não é quadrada"



transpos mat 
     | matQuad mat = [ (transfMat mat)!!j!!i | i <- [0..tamanhoMat mat -1], j <- [0..tamanhoMat mat -1]]
     | otherwise = error "Matriz não é quadrada"















