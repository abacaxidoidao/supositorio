{-
Varios espetaculos estao sendo apresentados em um grande teatro da cidade. Para cada um dos
espetaculos, registra-se o mapa de ocupacao da plateia, conforme as vendas dos ingressos. A plateia
esta representada por m filas numeradas de 1 a m,
sendo que cada fila contem n cadeiras tambem numeradas de 1 a n.
Considere a seguinte representacao para os dados:

-}

{-

Lugar na plateia - (fila, cadeira), onde fila e representada por um inteiro de 1 a m
e cadeira, por um inteiro de 1 a n. Plateia - Lista de duplas (lugar, situacao) sendo que a situacao e: 1
para indicar lugar ocupado e 0 para indicar lugar vago.Teatro - Lista de duplas (espetaculo, plateia) onde
espetaculo e representado por um inteiro de 1 a p.

-}

exemplo :: Plateia
exemplo = [

           ((1,1),1),((1,2),0),((1,3),1),((1,4),1),((1,5),1),
           ((2,1),0),((2,2),0),((2,3),1),((2,4),1),((2,5),0),
           ((3,1),1),((3,2),1),((3,3),0),((3,4),1),((3,5),1),
           ((4,1),0),((4,2),1),((4,3),0),((4,4),1),((4,5),0),
           ((5,1),1),((5,2),1),((5,3),1),((5,4),0),((5,5),1)

          ]


-- ====FUNCOES AUXILIARES====

maiorP pls = maximum $ map fst $ map fst pls -- Maior valor possível para uma fileira

-------------------------------------------------------
type Vet        = (Int,Int)
type Plateia    = [(Vet, Int)]
type FilaL      = [(Int,[Int])]
type Teatro     = (Int, Plateia)


--1 - Dada uma plateia pls, descreva a quantidade total de lugares ocupados (totalOcup).

totalOcup :: Plateia -> Int
totalOcup pls = sum listaLugaresOcupados -- Soma dos valores de "situação"
    where
      listaLugaresOcupados = snd $ unzip pls -- Lista dos valores "situação"



-------------------------------------------------------

--2 - Dado um lugar lg e uma platéia pls, verifique se o lugar lg está livre (estaLivre).

estaLivre lg pls =  not . null $ [ True | ((m,n),s) <- pls, lg == (m,n), s == 0] -- Se um lugar possui "situação" 0, ele está livre
                                                                                 -- logo, será printado um valor qualquer na List
                                                                                 -- Comprehension e será checado se a lista final é nula

-------------------------------------------------------

--3 - Dado um lugar lg e uma platéia pls, verifique se existe algum vizinho lateral de lg que está livre (vizinhoLivre)

vizinhoLivre :: Vet -> Plateia -> Bool
vizinhoLivre lg pls = estaLivre (x,y+1) pls || estaLivre (x,y-1) pls
    where
      (x,y)   = lg            -- O lugar fornecido abstraído em tupla para usá-lo na funcão


-------------------------------------------------------

--4 - Dada uma fila fl e uma platéia pls, descreva a lista de cadeiras livres da fila fl (cadeirasLivresFila).

cadeirasLivresFila :: Int -> Plateia -> [Int]
cadeirasLivresFila fl pls = [ n | ((m,n),s) <- pls, fl == m, s == 0 ] -- Diminui "maior" pela soma das "situação" quando está ocupada




-------------------------------------------------------

--5 - Dada uma platéia pls, descreva a lista de cadeiras livres para cada fila (lugLivresFila).

lugLivresFila :: Plateia -> FilaL
lugLivresFila pls =
    let
      maior = maiorP pls
    in
      [ (m, [ n | n <- [1..maior], estaLivre (m,n) pls]) | m <- [1..maior] ] -- Uma List Comprehension dentro de outra,
                                                                             -- deste modo a saída fica possível [(Int,[Int])]
                                                                             -- Para cada valor de m, checa todos valore de n
                                                                             -- numa tupla (m,n) e checa se pertence à lista,
                                                                             -- retornando mais uma lista dentro dá tupla.



-------------------------------------------------------

--6 - Dada uma platéia pls, descreva a(s) lista(s) com o maior número de cadeiras livres (filaMaxLivre).

filaMaxLivre :: Plateia -> [Vet]
filaMaxLivre pls = [ (m,tamMax) | (m,vagas) <- (lugLivresFila pls), length (vagas) == tamMax ] -- Junta a fila com seu tamMax correspondente
    where
      tamMax = maximum $ map (length . snd) (lugLivresFila pls) -- Retorna a maior quantidade de lugares livres dentre as filas



-------------------------------------------------------

--7 - Dado um teatro trs e um espetáculo ep, descreva a sua platéia (plateiaEsp).

plateiaEsp :: Teatro -> Int -> Int
plateiaEsp trs ep
        | ep /= fst trs = error "Informações de entrada não são condizentes"
        | otherwise     = ep * (totalOcup $ (snd) trs)



-------------------------------------------------------

--8 - Dado um teatro trs, um espetáculo ep e uma fila fl, descreva a lista de cadeiras livres da fila fl (cadeirasLivresFilaEsp).

cadeirasLivresFilaEsp :: Teatro -> Int -> Int -> FilaL
cadeirasLivresFilaEsp trs ep fl
            | ep /= fst trs = error "Informações de entrada não condizem"
            | otherwise     = filter ((==fl) . fst) (lugLivresFila pls)
    where
      pls = snd trs
