--import qualified Data.Map as M
--import qualified Data.List as L
--
--
--
--mapa f xs = [ f x | x <- xs]
--
--filtra fb xs = [ x | x <- xs, fb x ]
--
--menor xs = [head [ x | x <- xs, null $ filtra (<x) xs]]
--
--ordenaLista xs = [ x | x <- xs,  not . null $ menor $ (filter (/=x) xs)]
--
--nub' xs = [ xs!!i | i <- [0..length xs -1], null $ filter (== xs!!i)(take (i) xs)]
--
module Haskzao


    (
    leo,
     vc,
     qm
    )
where

leo x = x + 1
vc x = x^2
qm x = x + x + x*2
