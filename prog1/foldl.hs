fat 0 = 1
fat x = x * fat (x-1)


soma n = foldl (+) 0 [ (1/fat x) | x <- [1..n]]
