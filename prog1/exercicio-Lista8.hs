-- Exercício 1

{-
 
a) Dadas duas strings xs e ys, verificar se xs é prefixo de ys. 

-}

ehPref _ [] = False
ehPref [] _ = True
ehPref (x:xs) (y:ys) = x == y && ehPref xs ys

{-

b) Dadas duas strings xs e ys, verificar se xs é sufixo de ys.

-}

reverse' [] = []
reverse' (x:xs) = reverse' xs ++ [x] -- inverte a string

ehSufx _ [] = False
ehSufx [] _ = True
ehSufx xs ys = a == b && ehSufx as bs
                    where 
                          (a:as) = reverse' xs
                          (b:bs) = reverse' ys

{-

c) Dadas duas strings xs e ys, verificar se xs é sublista de ys.

-}

ehSub [] _ = True
ehSub _ [] = False
ehSub xs@(a:as) ys@(b:bs) = if xs == take (length xs) ys then True else ehSub xs (drop 1 ys)


{-
 
d) Verificar se uma string é um palíndrome (isso acontece quando a string é a mesma quando lida da
esquerda para a direita ou da direita para a esquerda)

-}

ehPali xs = xs == reverse' xs

{-
 
e) Verificar se os elementos de uma lista são distintos.

-}

elem' _ [] = False
elem' k (x:xs) = k == x || elem' k xs -- implementando o elem


distL [] = False
distL (z:ys) = not (elem' z ys || distL ys)

{-

f) Dada uma lista de elementos, obter a lista dos mesmos elementos, porém com a ordem inversa

-}

inversa xs = reverse' xs

{-

g) Determinar a posição (ou posições) de um elemento x em uma lista xs, se ele ocorre na lista.

-}

boolToInt x = if x then 1 else 0

enum xs = zip [0..] xs -- Uma lista de tuplas com o index e seu respectivo número

posicoes _ [] = []
posicoes k (x:xs) = if (k == b) then a : posicoes k xs else posicoes k xs
    where
      ((a,b):ys) = enum (x:xs)




