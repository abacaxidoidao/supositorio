import Data.List (sort)

soma [] = 0
soma (x:xs) = x + soma xs
--_______________________________________________________________________--

maior [x] = x
maior (x:xs) = x `max` maior xs
--_______________________________________________________________________--

ocorre _ [] = False
ocorre k (x:xs)
   | x == k    = True
   | otherwise = ocorre k xs
--_______________________________________________________________________--

--extrai _ [] = []
--extrai k (x:xs) = (\acc l -> acc : l) (if x<k then x else extrai k xs) ([])

--_______________________________________________________________________--

satisfaz _ [] = []
satisfaz fn (x:xs) = if fn x then x : satisfaz fn xs else satisfaz fn xs 


--_______________________________________________________________________--


tWhile _ [] = []
tWhile fn (x:xs) = if not $ fn x then [] else x : tWhile fn xs


--_______________________________________________________________________--

dWhile _ [] = []
dWhile fn (x:xs) = if fn x then x : dWhile fn xs else []


--_______________________________________________________________________--

inverte [] = []
inverte (x:xs) = inverte xs ++ [x]

--_______________________________________________________________________--


inter [] _ = []
inter (x:xs) ys = if elem x ys then x : inter xs ys else inter xs ys


--_______________________________________________________________________--

retira [] xs = xs
retira _ [] = []
retira rs (x:xs) = if elem x rs then retira rs xs else x : retira rs xs


uni _ [] = []
uni [] _ = []
uni xs@(a:as) ys@(b:bs) = xs ++ retira xs ys

--_______________________________________________________________________--


ehPref [] _ = True
ehPref _ [] = False
ehPref (x:xs) (y:ys) = x == y && ehPref xs ys

--_______________________________________________________________________--


ehSub [] _ = True
ehSub _ [] = False
ehSub xs ys = xs == take (length xs) ys || ehSub xs (drop 1 ys)

--_______________________________________________________________________--


ins k [] = [k]
ins k xss@(x:xs) 
         | k < x     = k : xss
         | otherwise = [x] ++ ins k xs

 
--_______________________________________________________________________--


ordena [] = []
ordena ys@(x:xs) = ins x (ordena xs)








