foo0 = compare 2 (mod 17 2) == EQ
{-A saída desta funcão será "False", pois, primeiro: É calculado o mod de 17
por 2, que retorna o resto (1), depois compara com 2, geranto o resultado "GT",
a funcão em seguida pergutna se "GT" a "EQ", o que é falso.-}

foo1 = x + y
    where
    x = y
    y = 2


-- Não precisa fornecer parametros para essa funcão pois ela não necessita de entrada e tem uma saída fixa.




{-foo2 = x + y
     where
        x = y
      where
            y = 2-}
-- Retornará um erro pois não pode ter dois wheres

foo3 x = if( x <= 30 ) then 'D'
          else if( x <= 50 ) then 'C'
               else if( x <= 80 ) then 'B'
                    else 'A'

-- A funcão retornará valores dependendo do valor de x


{-a) Dados os pontos A (x1, y1), B (x2, y2) e P (x, y), verifique se o ponto P
pertence à reta definida pelos dois pontos A e B.-}
-- type Tupla = (Num, Num)
coef (xa, ya) (xb, yb) = (yb - ya) / (xb - xa)
-- pert :: Tupla -> Tupla -> Tupla -> String
pert a b p | coef a p == coef a b   = "Pertence"
           | otherwise              = "Nao pertence"

{-b) Determine, aproximadamente, quantas pedras foram utilizadas para construir
a maior pirâmide de Gize, sabendo que a altura da pirâmide é igual a 138 m; o
tamanho do lado da base é igual a 227 m e as dimensões da pedra de calcário são
0.9m x 0.8m x 2.0m -}

vP = 2370334

predas = let t = (0.9 * 0.8 * 2.0)
       in ceiling(vP / t)

{-qtt p | p * (0.9 * 0.8 * 2.0) < vP = qtt (p+1)
        | otherwise                  = p

predas = qtt 100000-}

{-d) Defina os operadores lógicos a seguir:
i)and: usando 2 parâmetros de entrada, que tenha o mesmo comportamento do operador &&-}

endi x y = if x == True
            then if y == True
             then True
             else False
             else False

--iii) notAnd: usando 2 parâmetros de entrada, a negação de and

nundi x y = if x == True
             then if y == True
               then False
               else True
               else True
{- v) impl : usando 2 parâmetros de entrada, a operação de implicação lógica
(dadas duas sentenças lógicas p e q, a  implicação é representada por p → q)-}

impl p q = if q == False
            then True
            else False

{-f) Escreva uma função que receba as notas de três provas e de um trabalho e
verifique se o aluno foi aprovado ou reprovado. Considere a média 5, e que a
média das provas equivale à 70% da nota final e o trabalho computacional, a 30%
da nota final.  A resposta deve ser uma tupla com as notas das três provas, do
trabalho, a média, e a string dizendo se o aluno foi aprovado ou reprovado.
da disciplina. -}

passou p1 p2 p3 t = (mP, p1, p2, p3, t, if mD > 5 then "Passou"
                                         else "Nao passou")
                    where
                        mP = (p1 + p2 + p3) / 3
                        mD = 0.7 * mP + 0.3 * t

{-j) Escreva uma função que recebe uma tupla no formato (‘sexo’,altura) e
retorna o peso ideal de uma pessoa de acordo com os dados abaixo:

Homens: peso = 72. 7 * altura - 58;
Mulheres: peso = 62. 1 * altura - 44. 7-}

pesoI (x, y) | x == "H" = (72.7 * y) - 58
             | x == "M" = (62.1 * y) - 44.7


{-k) Considere o cadastro de pessoas com informações como o nome, a idade, a
altura e o sexo. A função pess, dado um código que é um valor entre 1 e 10,
responde os dados da pessoa associada a esse código. A seguir,
escreva funções que façam o que se pede:-}

pess :: Int->(String, Int, Float, Char)
pess x |x== 1     = ("Lois", 17, 1.60, 'F')
       |x== 2     = ("Tiago", 24, 1.85, 'M')
       |x== 3     = ("Maria", 67, 1.55, 'F')
       |x== 4     = ("Julia", 33, 1.73, 'M')
       |x== 5     = ("Golias", 22, 1.93, 'F')
       |x== 6     = ("Leia", 28, 1.73, 'F')
       |x== 7     = ("Luke", 28, 1.85, 'M')
       |x== 8     = ("Frodo", 31, 1.25, 'M')
       |x== 9     = ("Jessica", 25, 1.74, 'F')
       |x== 10    = ("Bruce", 31, 1.79,'M')
       |otherwise = ("---",0, 0.0, '-')

numMaior = pess (xa, xb,_ ,_)
    
