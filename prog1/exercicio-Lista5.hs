 {-
Exercício 1:
Para cada uma das expressões abaixo, faça o que se pede:
I) Escreva descrições usando listas, para as seguintes list
as constantes:
a) múltiplos de 5 maiores que 0 e menores que 80;
b) meses de um ano;
c) número de dias por cada mês de um ano;
d) dias da semana;
e) relação das disciplinas em que você está matriculado.
-}

mult5 = [ x | x <- [0..79], mod x 5 == 0 ]
meses = [ x | x <- [1..12] ]
diasM = [ x | x <- [31,28,31,30,31,30,31,31,30,31,30,31]]
diasS = [ x | x <- ["Segunda","Terca","Quarta","Quinta","Sexta","Sabado","Domingo"]]
discp = [ x | x <- ["Prog", "ATC", "Calculo", "Algebra linear", "IntroComp"]]


{-
Exercício 2:
Escreva um script com as definições das funções a seguir, de maneira que:
 i) identifique e utilize, quando necessário, a modularização
ii) sejam definições genéricas
iii) use definição local apenas quando necessário (promovendo a legibilidade do programa)
iv) comente seu código sempre que possível
v) resolva utilizando descrição por listas

a) Obter o menor valor de uma lista de números.
-}

menor xs = head [ x | x <- xs, null (menores x xs) ]

--Funcoes relevantes__________________________________________________________--

mapa f xs = [f x | x <- xs]

filtra fb xs = [x | x <- xs, fb x]

menores n xs = filtra (<n) xs

maiores n xs = filtra (>n) xs

--____________________________________________________________________________--

--Implementando a funcan elem_________________________________________________--
elem' y xs = not $ null [True | x <- xs, y == x]

--____________________________________________________________________________--

--Funcao maior________________________________________________________________--
maior xs = head [ x | x <- xs, null (maiores x xs)]

--____________________________________________________________________________--
{-
b) Dada uma lista
xs, fornecer uma dupla contendo o menor e o maior elemento dessa lista
-}

tuplaMM xs = (menor xs, maior xs)


{-
c) Produzir uma lista dos múltiplos de um dado número, menores ou iguais
a um dado limite lim. Exemplo: g 5 20 -> [5, 10, 15, 20]
-}

multn n l = [ n | n <- [n,2*n..l]]


{-
d)
Dividir uma lista pela metade e apresentar cada uma das partes em
uma dupla. Exemplo: divideLista [1,3,5,8,15] = ([1,3],[5,8,15] )
-}
--Implementando Take/Drop_____________________________________________________--

take' n xs | n > length xs = xs
           | otherwise     = [xs!!i | i <- [0..n-1]]

drop' n xs | n > length xs = []
           | otherwise     = [xs!!i | i <- [n..length xs-1]]
--____________________________________________________________________________--

divideLista xs = (take' m xs, drop' m xs)
                where
                  m = div (length xs) 2

divideLista' xs = ((take m xs), (drop m) xs)
                where
                  m = div (length xs) 2
--____________________________________________________________________________--

{-
e) Duplicar os elementos de uma lista. Exemplo: duplicaLista [1,2,3] -> [1,1,2,2,3,3]
-}

duplicaLista xs = concat [ [x,x] | x <- xs]

--____________________________________________________________________________--

{-
f) Dadas duas listas de elementos distintos, determinar a união delas.
-}


uni xs ys = [ x | x <- xs, not $ elem' x ys ] ++ ys

--____________________________________________________________________________--

{-
g) Dadas duas listas de elementos distintos, determinar a interseção delas.
-}

inter xs ys = [ x | x <- xs, elem' x ys]

--____________________________________________________________________________--
{-
h) Calcule a distância de Hamming entre dois números inteiros que possuam, cada um, exatamente
n algarismos. A distância de Hamming corresponde ao número de algarismos que diferem em suas
posições correspondentes
-}

--Funcão múltiplos de 10______________________________________________________--

mult10 n = take n $ iterate (*10) 1

--Para usar essa funcão, precisa-se fornecer a quantidade de algarismos que dos números que você quer como entrada

distHamming' xs ys n = sum [ 1 | x <- (reverse $ mult10 n), mod (div xs x) 10 /= mod (div ys x) 10]  





distHamming xs ys | length xs == length ys = sum [ 1 | i <- [0..length xs-1], (xs!!i) /= (ys!!i)]
                  | otherwise              = -1
--____________________________________________________________________________--

{-
i) Dada uma lista l, contendo uma quantidade igual de números inteiros pares e   ímpares (em
qualquer ordem), defina uma função que, quando avaliada, produz uma lista na qual esses números
pares e ímpares encontram-se alternados. Exemplo: alternaLista [10,2,31,45,6,18,5,20,15,19] ->
[10,31,2,45,6,5,18,15,20,19]
-}

alternaLista xs
    | length pares == length impares = concat [[pares!!i, impares!!i] | i <- [0..(div (length xs) 2) - 1]]
    | otherwise = [-1]
              where
                pares = filtra even xs
                impares = filtra odd xs
--____________________________________________________________________________--

{-
k) Verificar se um caracter dado como entrada é uma letra.
-}

ehLetra x = elem' x (['a'..'z'] ++ ['A'..'Z'])

--____________________________________________________________________________--

{-
l) Verificar se um caracter dado como entrada é um dígito.
-}

ehNum x = elem' x [0..9]

--____________________________________________________________________________--

{-
m) Verificar se uma cadeia de caracteres é uma palavra (ou seja, é formada apenas de letras).
-}

palavras xs = null [ x | x <- xs, not (ehLetra x)] && not (xs == "")

--____________________________________________________________________________--

{--
n) Verificar se uma cadeia de caracteres representa um número inteiro positivo (ou seja, a cadeia de
caracteres só é formada por dígitos). -}

digitos xs = null [ x | x <- xs, not (ehNum x)]
--____________________________________________________________________________--
{-
o) Dada uma cadeia de caracteres, contar o número de ocorrências de vogais, para cada vogal.
-}

--Funcoes axiliares___________________________________________________________--

ehVogal x = elem' x vogs
vogs = "aeiou" ++ "AEIOU"


--Funcao que conta as vogais__________________________________________________--

conta xs c = length [ True | x <- xs, x == c ]

contaVogais xs = [ conta xs x | x <- vogs]

totalVogal xs = zipWith (+) (mins) (mais)
    where
        a = contaVogais xs
        mais = drop' 5 a
        mins = take' 5 a














































--Codigo super otimizado do Kelvin____________________________________________--

vogais_min = "aeiou"
vogais_mai = "AEIOU"

-- filtraVogais = filtra ehVogal

-- conta xs x = length $ filtra (==x) xs



-- contaVogaisdoKelvin xs = zipWith (+) ma mi
--     where
--         vs = filtraVogais xs
--         ma = mapa (conta vs) vomais_mai
--         mi = mapa (conta vs) vogais_min

contaVogaisSatanicamente xs = map ($xs) . map contaSe  $  (zipWith orEq vogais_min vogais_mai)

orEq a b x = x == a || x == b

contaSe fn xs = length $ filtra fn xs




--
