-- Escreva um script com as definições das funções a seguir, de maneira que:
-- i) identifique e utilize, quando necessário, a modularização
-- ii) sejam definições genéricas
-- iii) use definição local apenas quando necessário (promovendo a legibilidade do programa)
-- iv) comente seu código sempre que possível

-- a) Dado um número inteiro, verifique se ele pertence ao intervalo (0,100) e é divisível por 3 e por 5.

-- int = [ x | x <- [0..100], mod x 3 == 0, mod x 5 == 0]  estão faltando 2 elses

-- numInt x = x >= 0 && x <= 100
--            then if mod x 3 == 0 && mod x 5 == 0
--              then "Pertence"

entre0e100 n = n <= 100 && n >= 0

divisivel n = mod n 3 == 0 && mod n 5 == 0

vmotestah n = entre0e100 n && divisivel n

-- b) O operador || (OU inclusivo), mapeia dois valores booleanos a e b em True quando pelo menos
-- um deles é True e em False caso contrário. Escreva uma função que denominaremos de oux (OU
-- exclusivo) que se assemelha ao OU inclusivo mas que mapeia em False quando ambos valores são
-- True


oux x y | x && y  = False
        | x || y  = True
        | otherwise = False


-- c) Dados a data inicial e final de um projeto, informe o número total de dias para a sua execução.


-- Ano bissexto?
bisse a = mod a 4 == 0 && not( mod a 100 == 0 ) || mod a 100 == 0


-- Tamanho do ano
anoTam a = if bisse a then 366 else 365



-- Checa qual o mês e retorna o número de dias desse mês
mes bs m  | m == 01   = 31
          | m == 02   = if bs then 29 else 28
          | m == 03   = 31
          | m == 04   = 30
          | m == 05   = 31
          | m == 06   = 30
          | m == 07   = 31
          | m == 08   = 31
          | m == 09   = 30
          | m == 10   = 31
          | m == 11   = 30
          | m == 12   = 31
          | otherwise = -1


-- Calcula quantos dias tem entre o mês dado do ano previsto para acabar o projeto até Janeiro deste ano



entreM2 m2 a2 = if m2 > 01 && m2 <= 12
                  then mes (bisse a2) m2 + entreM2 (m2-1) a2
                  else 0


-- Calcula quantos dias entre a primeira data até Dezembro deste ano


entreM1 m1 a1 = if m1 <= 12
                  then mes (bisse a1) m1 + entreM1 (m1+1) a1
                  else 0



-- Checa se os anos entre os anos dados de entrada são bissextos e soma o número de dias

entreAnos a1 a2 = if a2 == a1
                  then 0
                  else if a2 > a1
                    then anoTam a1 + (entreAnos (a1+1) a2)
                    else -1 * (anoTam a2 + (entreAnos (a2+1) a1))








--diasT d1 d2 m1 m2 a1 a2 = (d2 - d1) + (entreMeses m1 m2 a1 a2) + entreAnos a1 a2










-- d) Dados três comprimentos de lados l1, l2 e l3, verifique se podem formar um triângulo.

compri a b c = lado1 || lado2 || lado3
               where
                 lado1 = a + b > c && a + c > b
                 lado2 = b + c > a && b + a > c
                 lado3 = c + a > b && c + b > c


-- e) Calcule o maior de três números.

maior a b c = if a > b && a > c
               then a
               else if b > c && b > a
                then b
                else c



-- f) Dados três números inteiros distintos, calcule o quadrado do sucessor do maior número.

suce a b c = (succ(maior a b c))^2

-- g) A empresa Lucro Certo decidiu dar a seus funcionários um abono de natal. A gratificação será
-- baseada em dois critérios: o número de horas extras trabalhadas e o número de horas que o
-- empregado faltou ao trabalho. O critério estabelecido para calcular o prêmio é: subtrair dois terços
-- das horas que o empregado faltou de suas horas extras, obtendo um valor que determina o número
-- de pontos do funcionário. Faça uma função para calcular o abono de natal para cada funcionário. A
-- distribuição do prêmio é feita de acordo com a tabela abaixo:

--Pontos obtidos        Prêmio em R$

-- 1 a 10                  100,00
-- 11 a 20                 200,00
-- 21 a 30                 300,00
-- 31 a 40                 400,00
-- A partir de 41          500,00

pontos h f = h - 2 / 3 * ( f )

abonoNatal h f = if a < 1
             then 0
              else if a < 11
               then 100
               else if a < 21
                 then 200
                 else if a < 30
                   then 300
                   else if a < 40
                     then 400
                     else 500
            where
                a = pontos h f

--h) Considere que o preço de uma passagem de avião em um trecho pode variar dependendo da
-- idade do passageiro. Pessoas com 60 anos ou mais pagam apenas 60% do preço total. Crianças até
-- 10 anos pagam 50% e bebês (abaixo de 2 anos) pagam apenas 10%. Faça uma função que tenha
-- como entrada o valor total da passagem e a idade do passageiro e produz o valor a ser pago.

passa p i | i < 2 = 0.1 * p
          | i <= 10 = 0.5 * p
          | i > 60 =  0.6 * p
          | otherwise = p

-- i) Refaça o exemplo de cálculo de equações de 2º grau, calculando apenas as raízes reais da equação
-- e emitindo uma mensagem de texto, caso contrário

bas a b c = b^2 - 4 * a * c

raiz a b c | sqrt (delta) >= 0    = show ( ((-b) + delta) / (2*a) , ((-b) - delta) / (2*a) )
           | otherwise            = "Nao existem raizes reais"
    where delta = bas a b c

-- show = retorna a representação de qualquer objeto em string


-- j) Dada a idade de uma pessoa em segundos e um planeta do sistema solar, calcule qual seria a
-- idade relativa dessa pessoa no planeta informado, sabendo que o Período Orbital é o intervalo de
-- tempo que o planeta leva para executar uma órbita em torno do Sol (o que é denominado de ano,
-- que na Terra tem aproximadamente 365,25 dias). Considere então as informações abaixo para outros planetas do sistema solar:

-- Terra -> período orbital: 365.25 dias na Terra, ou 31557600 segundos
-- Mercúrio -> período orbital: 0.2408467 anos na Terra
-- Vênus -> período orbital: 0.61519726 anos na Terra
-- Marte -> período orbital: 1.8808158 anos na Terra
-- Júpiter -> período orbital: 11.862615 anos na Terra
-- Saturno -> período orbital: 29.447498 anos na Terra
-- Urano -> período orbital: 84.016846 anos na Terra
-- Netuno -> período orbital: 164.79132 anos na Terra

pOrbit i p  | p == 1     = show (idade / 0.2408467)
            | p == 2     = show (idade / 0.61519726)
            | p == 3     = show (idade / 1.8808158)
            | p == 4     = show (idade / 11.862615)
            | p == 5     = show (idade / 29.447498)
            | p == 6     = show (idade / 84.016846)
            | p == 7     = show (idade / 164.79132)
            | otherwise  = "Nao existe tal planeta"
      where idade = (i / 3155760)














-- ( ͡° ͜ʖ ͡°)








-- entreMeses m1 a1 m2 a2   | m2 == m1    = c1 + c2
--                          | m2 > m1     = mesf m1 + (entreMeses (m1+1) a1 m2 a2)
--                          | otherwise   = -1 * (mesf m1 + (entreMeses (m1+1) m2 a1))
--             where
--               mesf = (mes False)
--               cf a m = if bisse a && m >= 3 then 1 else 0
--               c1 = -(cf a1 m1)
--               c2 = (cf a2 m2)
