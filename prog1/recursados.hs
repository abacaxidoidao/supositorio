pertence _ [] = False
pertence k (x:xs) = if k == x then True else pertence k xs
--__________________________________________________________--


extrai _ [] = [] 
extrai k (x:xs) = extrai' k [] where
              extrai' k acc
                | x <= k    = acc ++ [x] ++ (extrai k xs)
                -- | x > k     = extrai' k acc
                | otherwise = extrai k xs 
                
extrai'' _ [] = []
extrai'' k (x:xs) = if x <= k then x : extrai'' k xs else extrai'' k xs

--__________________________________________________________--

satisfaz _ [] = []
satisfaz fn (x:xs) = satiz fn [] where
                satiz f acc
                   | fn x      = acc ++ [x] ++ (satisfaz fn xs)
                   | otherwise = satisfaz fn xs

satisfaz' _ [] = []
satisfaz' fn (x:xs) = if fn x then x : satisfaz' fn xs else satisfaz' fn xs

--__________________________________________________________--

insere k [] = [k]
insere k (x:xs) = if k > x then x : insere k xs else x : k : xs



--__________________________________________________________--

-- foldl' :: Foldable t => (a -> a -> a) -> a -> [a] -> [a]
foldl' fn k [] = fn k 1
foldl' fn k (x:xs) = fn k (foldl' fn x xs)
            

--__________________________________________________________--

{-
Fazer uma função que recebe como parâmetro um inteiro n positivo e retorne o valor do seguinte somatório: 

E n i==0 (l/i!)

-}












