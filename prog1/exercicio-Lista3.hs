-- a) Dado um ponto P(x,y) do plano cartesiano, defina funções que descrevam
-- a sua pertinência nas regiões abaixo:
-- i) Um retângulo (com lados paralelos aos eixos cartesianos) dado pelas coordenadas do
-- canto superior esquerdo e do canto inferior direito, como mostrado na figura.

entre x a b = x >= a && x <= b


pertR (x1, y1) (x2, y2) (x, y) = (entre x x1 y1) && (entre y y1 y2)


-- ii) Um losango , com os seus eixos paralelos aos eixos cartesianos,
-- dado pelas coordenadas do vértice esquerdo e do vértice superior, como mostrado na figura

coef x1 x2 y1 y2 = y2 - y1 / (x2 - x1)

reta x x1 x2 y y1 y2 = coef x1 x2 y1 y2 * (x - x1) - y

pertL (x, x1, x2, y, y1, y2)      | r >= 0 = (r, "Crescente", if r <= y then "Pertence" else "Nao pertence")
                                  | r <= 0 = (r, "Decrescente", if r >= y then "Pertence" else "Nao pertence")
                                  where
                                    r = reta x x1 x2 y y1 y2







-- iii) Um círculo dado pelo seu centro C e seu raio r
hipo b c = sqrt(b^2 + c^2)

circ x y r = hipo x y <= r

-- b) Defina funções que implementem as seguintes especificações:
-- i) Dados três números a, b e c. Determine quais das seguintes relações eles mantém entre si:
-- a) os três são diferentes;
-- b) apenas dois são iguais;
-- c) os três são iguais.

num a b c | a == b && b == c = "Iguais"
          | a == b && b /= c || b == c && a /= c || a == c && b /= a = "2 iguais"
          | otherwise = "Nao iguais"



-- ii) Dados o canto superior esquerdo e o canto inferior direito de um retângulo R,
-- paralelo aos eixos, determine quantos quadrantes são cobertos por ele

quadra (x, y) (x2, y2) (x1, y1) | (entre x x1 x2) && (entre y y1 y2)             = "Quadrante 1"
                                | (entre x (-x1) (-x2)) && (entre y y1 y2)       = "Quadrante 2"
                                | (entre x (-x1) (-x2)) && (entre y (-y1) (-y2)) = "Quadrante 3"
                                | (entre x x1 x2) && (entre y (-y1) (-y2))       = "Quadrante 4"
                                | otherwise                                      = "Erro"

ret = quadra (0, 0)


-- c) Calcule o IMC dados o peso em Kg e a altura em m de uma pessoa e retorne o resultado
-- em uma mensagem de texto. Verifique como é feito o cálculo do IMC e qual a classificação
-- adotada.


imc (k, h) = (k / (h * h), "eh o seu IMC", if v < 18.5 then "abaixo do peso"
                                            else if v < 24.9 then "peso ideal"
                                              else if v < 29.9 then "levemente acima do peso"
                                                else if v < 34.9 then "obesidade grau 1"
                                                  else if v < 39.9 then "obesidade grau 2"
                                                    else "obesidade morbida")
                                                    where v = k / (h * h)






-- d) Calcule o desconto do imposto de renda, segundo a tabela abaixo, dado o total de ganhos de
-- uma pessoa em reais

       -- Faixa Salarial                     Desconto
       --      até 500                          0%
       --  500 até 1500                         10%
       -- 1500 até 2500                         15%
       --    > 2500                             25%


descI s | s <= 500      = s
        | s <= 1500     = s * 0.9
        | s <= 2500     = s * 0.85
        | otherwise     = s * 0.75



-- e) Dado três pontos no plano cartesiano, determine se o triângulo formado por estes pontos é
-- equilátero, isoscéles ou escaleno.

-- funcCompl (x1, y1) (x2, y2) (x3, )
--
-- triC (x1, y1) (x2, y2) (x3, y3) |


-- f)  Considerando um tabuleiro de xadrez convencional (8x8) e a posição de duas rainhas neste
-- tabuleiro, determine se uma rainha pode capturar a outra.


-- X representa a posicão da rainha1 em relacão ao eixo horizontal do tabuleiro, onde o máximo é 8. Y representa a posicão da rainha1 em relacão ao eixo vertical, onde o máximo é 8. A e B representam a posicão da rainha2 em relacão ao eixo horizontal e vertical respectivamente, onde o máximo é 8.
-- A funcão abaixo calcula quantos movimentos serão necessários uma raiha chegar à outra quanto elas estão alinhadas no eixo x.


mov1 y b     | t         = 1 + mov1 (y-1) b
             | u         = 1 + mov1 (y+1) b
             | otherwise = 0
        where
        t = y - b > 0
        u = b - y > 0

mov2 x a     | t         = 1 + mov2 (x-1) a
             | u         = 1 + mov2 (x+1) a
             | otherwise = 0
        where
        t = x - a > 0
        u = a - x > 0

movT x y a b = (mov1 y b) + (mov2 x a)
