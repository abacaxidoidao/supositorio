#include <stdio.h>
#include <stdlib.h>
#include "libsort.h"

#define CUTOFF 20

void insort(Item *a, int lo, int hi) {
    for (int i = hi; i > lo; i--) {
        compexch(a[i-1], a[i]);
    }
    for (int i = lo+2; i <= hi; i++) {
        int j = i;
        Item v = a[i];
        while (less(v, a[j-1])) {
            a[j] = a[j-1];
            j--;
        }
        a[j] = v;
    }
}

void merge(Item *a, Item *aux, int lo, int mid, int hi) {
    for (int k = lo; k <= hi; k++)
        aux[k] = a[k];               // Copy array
    int i = lo, j = mid+1;
    for (int k = lo; k <= hi; k++) { // Merge
        if      (i > mid)              a[k] = aux[j++];
        else if (j > hi)               a[k] = aux[i++];
        else if (less(aux[j], aux[i])) a[k] = aux[j++];
        else                           a[k] = aux[i++];
    }
}

#define SZ2      (sz+sz)
#define MIN(X,Y) ((X < Y) ? (X) : (Y))

void sort(Item *a, int lo, int hi) {
    int N = (hi - lo) + 1;
    int y = N - 1;
    Item *aux = malloc(N * sizeof(Item));
    for (int sz = 1; sz < N; sz = SZ2) {
        for (int lo = 0; lo < N-sz; lo += SZ2) {
            if (N-sz < CUTOFF){
                insort(a, lo, N-sz);
            }
            else{
                int x = lo + SZ2 - 1;
                merge(a, aux, lo, lo+sz-1, MIN(x,y));
            }
        }
    }
    free(aux);
}
