#include <stdio.h>
#include <stdlib.h>
#include "libsort.h"

extern void sort(Item *a, int lo, int hi);

int main(int argc, char const *argv[]){
    int tam = atoi(argv[1]);
    int *vec = malloc(sizeof(int) * tam);
    for (int i = 0; i < tam; i++){
        fscanf(stdin, "%d", &vec[i]);
    }
    sort(vec, 0, tam-1);
    for (int i = 0; i < tam; i++){
        printf("%d\n", vec[i]);
    }
    free(vec);
    return 0;
}
