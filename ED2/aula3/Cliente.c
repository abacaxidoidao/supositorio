#include <stdio.h>
#include <stdlib.h>
#include "item.h"

int * init_arr(int N){
  int *arr = malloc(sizeof(int)*N);
  return arr;
}

void read_stdin(int *arr, int N){
  for (int i = 0; i < N; i++)
    fscanf(stdin, "%d", &arr[i]);
}

void print(int *arr, int N){
  for (int i = 0; i < N; i++)
    printf("%d\n", arr[i]);
}

void destroy(int *arr){
  free(arr);
}

void insertion(int *arr, int N){
  for (int i = 1; i < N; i++){
    int key = arr[i];
    int newPos = i - 1;
    while (newPos >= 0 && arr[newPos]) {
      arr[newPos + 1] = arr[newPos];
      newPos--;
    }
    arr[newPos + 1] = key;
  }
}

//computes the lesser value of an array from a start point
int selection_helper(int *arr, int N, int pos){
  int min = pos;
  for (int i = pos+1; i < N; i++)
    if (arr[i] < arr[min])
      min = i;
  return min;
}

//select sorts
void selection(int *arr, int N){
  for (int i = 0; i < N-1; i++){
    int minPos = selection_helper(arr, N, i);
    compexch(arr[i], arr[minPos]);
  }
}

void bubble(int *arr, int N){
  int maxPos = N;
  for (int i = 0; i < N-1; i++) {
    for (int j = 0; j < maxPos - 1; j++)
      compexch(arr[j], arr[j+1]);
    maxPos--; //lessens the maximum position of the array since it wont be needed to sort from there now
  }           //avoids unnecessary loops
}

void shaker(int *arr, int N){
  int maxPos = N; //the minimum and maximum values to iterate and sort
  int minPos = 0; //avoids unnecessary loops
  for (int i = 0; i < N-1; i++){
    for (int j = minPos; j < maxPos - 1; j++)
      compexch(arr[j], arr[j+1]);
    maxPos--;
    for (int k = maxPos - 1; k > minPos; k--)
      compexch(arr[k-1], arr[k]);
    minPos++;
  }
}

int main(int argc, char *argv[]){
  int N = atoi(argv[1]);
  // printf("Até N: ");
  // scanf("%d", &N);
  // 
  int *arr = init_arr(N);
  
  read_stdin(arr, N);
  // print(arr, N);
  // printf("\n");
  insertion(arr, N);
  // selection(arr, N);
  // bubble(arr, N);
  // shaker(arr, N);
  // print(arr, N);
  destroy(arr);
}