#ifndef UF_H_
#define UF_H_

void UF_init(int);

void UF_union(int, int);

int UF_find(int);

int UF_connected(int, int);

#endif //UF_H_