#include <stdio.h>
#include <stdlib.h>

int josefofast(int ppl, int iterator){
    if (ppl == 1) return 1;
    else
        return (josefofast(ppl - 1, iterator) + iterator - 1) % ppl + 1;
}

int main(){
    int N, iter;
    printf("Numero de pessoas: ");
    scanf("%d", &N);
    printf("Iterador: ");
    scanf("%d", &iter);

    printf("%d\n", josefofast(N, iter));

    return 0;
}