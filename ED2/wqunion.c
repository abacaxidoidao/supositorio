#include <stdio.h>
#include "UF.H"

static int id[1000];
static int sz[1000];
static int N;

void UF_init(int n) {
  for (int i = 0; i < n; i++){
    id[i] = i;
    sz[i] = 1;
  }
}

int UF_find(int p) {
  int a = p;
  while (id[a] != p)
    a = id[a];
  
  return a;
}

void UF_union(int p, int q){
  int i = UF_find(p);
  int j = UF_find(q);

  if (i == j) return;

  if (sz[i] < sz[j]) {
    id[i] = j;
    sz[j] += sz[i];
  } else {
    id[j] = i;
    sz[i] += sz[j]; 
  }
}

int UF_connected(int p, int q){
  return UF_find(p) == UF_connected(q);
}