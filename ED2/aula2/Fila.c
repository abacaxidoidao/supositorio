#include <stdio.h>
#include <stdlib.h>
#include "Fila.h"
#include "BST.h"

struct queue{
   It_t *head;
   It_t *tail;
};

struct it{
    Bst_t *node;
    It_t *next;
    It_t *ant;
};

It_t * init_it(Bst_t *node){
    It_t *it = malloc(sizeof(It_t));
    it->node = node;
    it->next = it->ant = NULL;

    return it;
}

void destroy_it(It_t *it){
    free(it);
}

void enqueue(Queue_t *queue, Bst_t* node){
    It_t *it = init_it(node);

    if (!queue->tail)
        queue->tail = queue->head = it;
    else{
        it->next = queue->tail;
        queue->tail->ant = it;
        queue->tail = it;
    }
}

Bst_t * dequeue(Queue_t *queue){
    if (queue->head == NULL) return NULL;
    It_t *ret = queue->head;
    Bst_t *node = ret->node;

    if (queue->tail == queue->head)
        queue->tail = queue->head = NULL;
    else {
        queue->head = queue->head->ant;
        queue->head->next = NULL;
    }
    destroy_it(ret);
    return node;
}