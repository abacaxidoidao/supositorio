#include <stdio.h>
#include <stdlib.h>
#include "Pilha.h"
#include "BST.h"

struct it{
    Bst_t *node;
    It_t *next;
    It_t *ant;
};

struct stack{
    It_t *top;
    It_t *first;
};

int is_empty(Stack_t *stack){
    return stack->top == NULL;
}

Stack_t * init_stack(void){
    Stack_t *stack = malloc(sizeof(Stack_t));
    stack->top = stack->first = NULL;

    return stack;
}

It_t * init_it(Bst_t *node){
    It_t *it = malloc(sizeof(It_t));
    it->node = node;
    it->next = it->ant = NULL;

    return it;
}

void destroy_it(It_t *it){
    free(it);
}

void destroy_stack(Stack_t *stack){
    It_t *stk1 = NULL;
    It_t *stk2 = stack->first;
    while (stk2){
        stk1 = stk2;
        stk2 = stk2->next;

        destroy_it(stk1);
    }
    free(stack);
}

void push(Stack_t *stack, Bst_t *node){
    It_t *it = init_it(node);

    if (!stack->first)
        stack->first = stack->top = it;
    else{
        stack->top->next = it;
        it->ant = stack->top;
        stack->top = it;
    }
}

Bst_t * pop(Stack_t *stack){
    if (stack->top == NULL) return NULL;
    It_t *ret = stack->top;
    Bst_t *node = ret->node;
    
    if (stack->top == stack->first)
        stack->first = stack->top = NULL;
    else{
        stack->top = stack->top->ant;
        stack->top->next = NULL;
    }

    destroy_it(ret);
    
    return node;
}

