#include "BST.h"

#ifndef FILA_H_
#define FILA_H_

typedef struct queue Queue_t;

typedef struct it It_t;

It_t * init_it(Bst_t *node);

void destroy_it(It_t *it);

#endif //FILA_H_