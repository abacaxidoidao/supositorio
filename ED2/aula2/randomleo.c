#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "randomleo.h"

int * random_arr(int N){
  int *arr = malloc(sizeof(int)*N);

  for (int i = 0; i < N; i++){
    arr[i] = rand();
  }
  return arr;
}

void destroy_arr(int *arr){
  free(arr);
}
