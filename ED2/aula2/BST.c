#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "BST.h"
#include "randomleo.h"
#include "Pilha.h"
#include <time.h>

struct bst{
  Bst_t *l, *r;
  int info;
};

Bst_t * create_null(){
  return NULL;
}

Bst_t * create(int info){
  Bst_t *n = malloc(sizeof(Bst_t));
  n->l = n->r = NULL;
  n->info = info;
  return n;
}

Bst_t * insert(Bst_t *node, int info){
  if (!node)
    node = create(info);
  else if (info < node->info)
    node->l = insert(node->l, info);
  else if (info > node->info)
    node->r = insert(node->r, info);

  return node;
}

void print(Bst_t *node){
  if (!node)
    return;
  
  print(node->l);
  printf("%d\n", node->info);
  print(node->r);
}

void non_recursive_print(Bst_t *node){
  Stack_t *stack = init_stack();
  if (node)
    push(stack, node);
  else return;

  while (!is_empty(stack)){
    Bst_t *preorder = pop(stack);
    printf("%d\n", preorder->info);

    if (preorder->r)
      push(stack, preorder->r);
    if (preorder->l)
      push(stack, preorder->l);
  }
  destroy_stack(stack);
}

int non_recursive_depth(Bst_t *node){
  Stack_t *stack = init_stack();
  if (node)
    push(stack, node);
  else return 0;

  int lDepth = 1, rDepth = 1;

  while (!is_empty(stack)){
    Bst_t *preorder = pop(stack);

    if (preorder->r) {
      push(stack, preorder->r);
      rDepth++;
    }
    if (preorder->l){
      push(stack, preorder->l);
      lDepth++;
    }
  }
  destroy_stack(stack);
  return lDepth > rDepth ? lDepth : rDepth;
}

int depth(Bst_t *node){
  if (!node)
    return 0;
  
  int lDepth = depth(node->l);
  int rDepth = depth(node->r);

  if (lDepth > rDepth)
    return lDepth + 1;
  return rDepth + 1;
}

void destroy(Bst_t *node){
  if (!node)
    return;

  destroy(node->l);
  destroy(node->r);
  free(node);
}

int main(int argc, char const *argv[]){
  Bst_t *a = NULL;
  srand(time(NULL));
  // a = insert(a, 3);
  int N;
  
  printf("Até N: ");
  scanf("%d", &N);
  int *arr = random_arr(5);
  a = insert(a, arr[0]);
  for (int i = 1; i < N; i++)
    insert(a, i);

  // print(a);
  printf("Non-recursive: %d\nRecursive: %d\n", non_recursive_depth(a), depth(a));
  
  non_recursive_print(a);
  destroy(a);
  destroy_arr(arr);

  return 0;
}
