#include "BST.h"

#ifndef PILHA_H_
#define PILHA_H_

typedef struct stack Stack_t;

typedef struct it It_t;

Stack_t * init_stack(void);

It_t * init_it(Bst_t *node);

void destroy_it(It_t *it);

void destroy_stack(Stack_t *stack);

void push(Stack_t *stack, Bst_t *node);

Bst_t * pop(Stack_t *stack);

int is_empty(Stack_t *stack);

#endif //PILHA_H_