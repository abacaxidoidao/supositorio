#include <stdio.h>
#include <stdlib.h>
#include "inreader.h"

String * create_string(int N, char *t){
  String *s = malloc(sizeof(String));
  s->length = N;
  s->text = malloc(sizeof(char) * N);
  for (int i = 0; t[i] != '\0'; i++)
    s->text[i] = t[i];
  return s;
}

void print_string(String *s){
  for (int i = 0; i < s->length; i++)
    printf("%c", s->text[i]);
  printf("\n");
}

void read_in(char *in){
  FILE *fp = fopen(in, "r");
  if (!fp){
    fprintf(stderr, "masoqueehiso\n");
    exit(1);
  }
  char len[20];
  fgets(len, 20, fp);
  int len_ = atoi(len);
  int c;

  char *text = malloc(sizeof(char) * (len_ + 1));
  int tam = 0;
  while ((c = fgetc(fp)) != EOF)
    text[tam++] = c;
  text[tam] = '\0';

  String *str = create_string(tam, text);
  print_string(str);
}