#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
    int vet[20];
    int tam;
}P;

int pop(P *p){
    return p->vet[--p->tam];
}

void push(P *p, int num){
    p->vet[p->tam++] = num;
}

int calc(void) {
    P *op = &(P){.vet = {0}, .tam = 0};
    int a, b, res;
    char line[256];
    fgets(line, 256, stdin);
    for(size_t i = 0; i < strlen(line); i=i+2){
        switch (line[i]){
            case '*':
                a = pop(op);
                b = pop(op);
                push(op, (a * b));
                break;
            case '+':
                a = pop(op);
                b = pop(op);
                push(op, (a + b));
                break;
            default:
                res = ((int)line[i] - '0');
                push(op, res);
                printf("%d\n", res);
                break;
        }
    }
    
    return pop(op);
}

int main() {
    // Le a entrada e calcula e retorna o resultado.
    int res = calc();
    // Exibe a saida.
    printf("\n%d\n", res);
}
