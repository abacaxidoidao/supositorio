#include <stdio.h>
#include <stdlib.h>

int indice(int *vet, int key){
    int i = 0;
    while (1)
        if (vet[i++] == key) return (i-1);
}

static int inI = -1;

void print_post_order(int *pre, int *in, int N) {
    inI++;
    int l, r;
    int split = indice(in, pre[inI]); //position to split into two arrays
    l = split;
    r = N - split - 1;

    //initializes the left-sub tree array
    if (l > 0){
        int L[l];
        for (int j = l-1; j >= 0; j--)
            L[j] = in[j];
        print_post_order(pre, L, l);
    }
    //initializes the right-sub tree array
    if (r > 0){
        int R[r];
        for (int j = split + 1; j < N; j++)
            R[j - (split+1)] = in[j];
        print_post_order(pre, R, r);
    }
    printf("%d ", in[split]);
}

typedef struct a{
    struct a *l, *r;
    int i;
} A;

A *i(int i, A *l, A *r){
    A *m = malloc(sizeof(A));
    m->l = l;
    m->r = r;
    m->i = i;

    return m;
}

void p(A *a){
    if (!a) return;
    //printf("%d ", a->i);
    p(a->l);
    // printf("%d ", a->i);
    p(a->r);
    printf("%d ", a->i);
}

int main() {
    // Le a entrada.
    int N;
    scanf("%d\n", &N);
    int *pre = malloc(N * sizeof(int));
    for (int i = 0; i < N; i++) {
        scanf("%d", &pre[i]);
    }
    int *in = malloc(N * sizeof(int));
    for (int i = 0; i < N; i++) {
        scanf("%d", &in[i]);
    }

    // A *ar = i(1, 
    //             i(2, 
    //                 i(4, NULL, NULL), i(5, NULL, NULL)) 
    //             , i(3, NULL, i(6, NULL, NULL)));
    // p(ar);

    // Calcula e exibe a saida.
    print_post_order(pre, in, N);
    printf("\n");
    free(pre);
    free(in);
}
