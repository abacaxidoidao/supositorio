#include <stdio.h>
#include <stdlib.h>

#define MAX 100

int stat[MAX] = {0};
float memo[MAX];

float f_C(int N) {
    // Implemente essa funcao para resolver a questao 3.
    if (N == 0) {
        return 1.0;
    }
    else if (stat[N]) {
        return memo[N];
    }
    else {
        float n = N;
        // printf("%f\n", n);

        float sum = 0;
        for (int k = 1; k <= N; k++) {
            sum += f_C(k-1);
            sum += f_C(N-k);
        }

        float res = n + sum/n;

        memo[N] = res;
        stat[N] = 1;

        return res;
    }
}

int main() {
    int N;
    // Le a entrada.
    scanf("%d\n", &N);
    // Calcula e exibe a saida.
    float res = f_C(N);
    printf("%f\n", res);
}
