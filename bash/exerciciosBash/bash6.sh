#!/bin/bash

echo "O caracter '>' é um redirecionamento de saída"
echo ""

echo "date > data.txt"
date > data.txt
cat data.txt

echo

echo "ls > data.txt"
ls > data.txt
echo "data.txt já existe e terá seu conteúdo substituido:"

echo
cat data.txt

echo

