#!/bin/bash

echo '
Until:

Until é o oposto do while, executa um bloco de código até que a condição dada se torne verdadeira;

until [expr]; do
	#CODE
done

Case:

case var in

Pattern | pattern ...) #CODE;;
Pattern | pattern ...) #CODE;;
*)#CODE;;
esac

Note que podem haver um ou mais padrões dentro de uma cláusula;
No primeiro padrão encontrado, será executado o bloco de código da cláusula;
As cláusulas tem de ser terminadas com ;;, porém há outras opções que não cabem serem mecionadas aqui;
*) serve como uma cláusula geral, ou defalt, caso nenhuma outra tenha sido executada. Sua inclusão é opcional.'

