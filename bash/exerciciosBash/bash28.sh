#!/bin/bash

echo '
For-loop:

for var in [lista]; do
	#CODE
done

A variável var vai receber um valor a cada loop dentro de [lista];
Caso in [lista] não for especificado, var atua sobre todos os parâmetros posicionais, como se fosse in "$@";
Bash permite o uso de break e continue;

meuVet=( {1..15} )
for num in ${!meuVet[@]}; do
	let meuVet[num]+=3
done
echo ${meuVect[@]}'
echo
echo

meuVet=( {1..15} )
for num in ${!meuVet[@]}; do
	let meuVet[num]+=3
done
echo ${meuVet[@]}
