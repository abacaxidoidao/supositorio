#!/bin/bash

echo -e "As diferenças entre aspas duplas e simples:
Partial quoting, ou citação parcial, (aspas duplas), preserva a maior parte do valor literal dos caracteres dentro das aspas duplas;
Com exceção para os caracteres especias $, \\ e crase;
Backslash, \\, só retém seu valor especial quado precedido de $, crase, aspas duplas, \\ ou uma nova linha.
Nesses casos \\ será removido da string dentro das aspas duplas.

str=teste
'echo \\$'str \\ contém: $'str"
str="teste"
echo "\$str \ contém: $str"

echo
echo "echo estou em: 'pwd'"
echo  "estou em: `pwd`"
echo

echo 'Full quoting, citação completa, todos os caracteres denro das aspas simples terão seu valor literal preservado;
Não se pode ter aspas simples dentro de aspas simples mesmo que precedida de \.

str="teste"
echo $str vale nada aqui'
echo '$str vale nada aqui'
echo
echo "ls 'D*'"
ls 'D*'
