#!/bin/bash

echo "
Em relação à strings, podemos utilizar os seguintes comandos:
-v: Verdadeiro caso o tamanho da string seja zero;
-n: Verdadeiro caso o tamanho de string NÃO seja zero;
=: Compara se duas strings são iguais;
> e <: Comparam se a string é ordenada depois, ou antes, respectivamente, em ordem da tabela ASCII;
Sempre bote espaços entre os operadores, os [[]] e as variáveis. "
echo

str="sei"
stra="Sei"
str2="Sey"

echo "str=sei
stra=Sei
str2=Sey
[[ '$'str = '$'stra ]]"

[[ '$'str = '$'stra ]]
echo $?
echo

echo "[[ ''$'str' < ''$'str2' ]]"
echo $?
