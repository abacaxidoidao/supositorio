#!/bin/bash

echo "
Operador de negação:
O operador '!' conhecido como operador de negação, pode negar qualquer expressão lógica em bash."
echo

echo "Evite usar essa forma
! [[ -d bashEx ]]"
! [[ -d bashEx ]]
echo $?
echo

echo "Sempre prefira esta
[[ ! 123 -eq 123 ]]"
[[ ! 123 -eq 123 ]]
echo $?
echo 

