#!/bin/bash

echo '
Wildcards:
São caracteres com valores e funções especiais úteis para representar uma possibilidade de strings que se queira, sem escrever todas uma a uma, são wildcards:
* ?: Representa um único caractere qualquer;
* *: Representa um ou mais caractere qualquer;
* []: Delimita uma sequência de caracteres, ao contrário de ? e *;
* {}: Expansão das strings ou caracteres dentro das chaves.

testes=( teste{1..9}.txt )
echo ${testes[@]}
echo "${testes[0]}" > testes1.txt
echo "${testes[1]}" > testes2.txt
echo "${testes[2]}" > testes3.txt'

testes=( teste{1..9}.txt )
echo ${testes[@]}
echo "${testes[0]}" > testes1.txt
echo "${testes[1]}" > testes2.txt
echo "${testes[2]}" > testes3.txt
echo

echo 'cat {testes1, testes2, testes3}.??? > TestesAll.txt
cat TestesAll.txt'


cat {testes1,testes2,testes3}.??? > TestesAll.txt
cat TestesAll.txt
echo '

ls -l [tT]estes*'
ls -l [tT]estes*
