#!/bin/bash

echo '
Como visto no exemplo anterior, as funções podem utilizar return ao invés de exit (que sairia do script), para retornar um valor de saída, exit status,
ou simplesmente terminar a função como mostrado no exemplo;
O valor indicado em return é associado a $?, com isso pode-se criar valores de saída personalizados para erros diferentes;

O valor de retorno da função pode ser atribuído a uma variável como:
var=$(foo)
'

cpDesktop () 
{
	if [[ -e ~/Desktop/"$1" ]]; then
		return 1
	else
		cp "$1" ~/Desktop/
		return 0
	fi
		
}


cpDesktop meutext.txt
if (($?==1)); then
	echo "Arquivo já existe"
fi
