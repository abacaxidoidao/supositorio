#!/bin/bash

echo '
While executa um bloco de comandos até que a condição dada seja falsa;

while [expr]; do
	#CODE
done

O exemplo a seguir mostra o uso do while para leitura de arquivos.

echo "Aqui" > exemp.txt
echo "exemplos" >> exemp.txt
echo "de linha" >> exemp.txt

Note o uso do comando read
while read linha; do
	let l+=1
	echo "$l: $linha"
done < exemp.txt'

echo


echo "Aqui" > exemp.txt
echo "exemplos" >> exemp.txt
echo "de linha" >> exemp.txt


while read linha; do
	let l+=1
	echo "$l: $linha"
done < exemp.txt

echo '
Este exemplo mostra como iterar sobre um arquivo, o aquivo é posto como entrada para o while (redirecionamento) e para cada iterção ele lê uma linha do arquivo, até
ele chegar ao fim.'
