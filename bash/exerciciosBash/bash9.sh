#!/bin/bash

echo "O comando 'read' é usado para ler entradas digitadas pelo usuário ou de arquivos e atribuir a uma variável"
echo

echo -e " '-a': entrada é armazenada em um vetor especificado; \n '-d': muda o delimitador de fim de leitura da entrada; \n '-p': exibe uma mensagem prompt antes de ler a entrada (só aparece a entrada se a menssagem vier do terminal); \n '-t': tempo até que read seja cancelado se não receber uma entrada do usuário ou pipe."

echo
echo

echo "Com mensagem de prompt, entrada armazenada em entry"


echo "read -p "Olá Leo: " entry"

read -p "Olá Leo: " entry
echo "Você digitou: "$entry""

echo


echo "Armazenada em array"
echo

echo "read -a entrada"

read -a entrada
echo "${entrada[2]}"

