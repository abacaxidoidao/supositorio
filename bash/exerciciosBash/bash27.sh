#!/bin/bash

echo '
Valor de retorno:

Todo script, comando, função ou teste de condição retornam um valor de retorno após seu término, ou exit code;
Esse valor é obtido por $? Como visto anteriormente:
 * 0: Para uma operação realizada com sucesso, ou true;
 * 1: Para uma falha na operação, ou false;
Para comandos, podem existir valores de saída diferentes de 0 e 1, leia o manual do comando;
Como percebido anteriormente também, o valor em $? é atualizado após cada operação.

Estruturas condicionais:

If-then-else:

if[expr]; then
	#código
elif[expr]; then
	#código
else
	#código
fi'
echo
echo 'mkdir MeuDir
if [[ $? -ne 0 ]]; then
	echo "Não foi possível criar diretório"
else
	echo "Diretório criado"
fi'
echo
echo
mkdir MeuDir
if [[ $? -ne 0 ]]; then
	echo "Não foi possível criar diretório"
else
	echo "Diretório criado"
fi

echo
echo
echo 'rm MeuDir
if [[ $? -ne 0 ]]; then
	echo "Faltou a opção -r"
fi'


rm MeuDir
if [[ $? -ne 0 ]]; then
	echo "Faltou a opção -r"
fi
