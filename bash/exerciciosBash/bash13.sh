#!/bin/bash

echo "Substituição de comandos:
Serve para executar um comando dentro das crases ou '$'() e o seu resultado ser utilizado em outro contexto, como outro comando ou ser guardado em uma variável;
Sempre opter por '$'(), melhor legibilidade e mais intuitivo, aninhamento de comandos mais fácil."
echo
echo
#echo "mkdir '$'(cat nomes.txt)"
mkdir $(cat datas.txt)
echo

echo "Aninhamento pouco intuitivo com backslash"
echo "'ls -lh (backslash)'cat bashEx(backslash)'' -> Onde aspas simples representa crase"

echo `ls -l \`cat nomes.txt\``
echo

echo "'$'z para preservar espaços e novas linhas na impressão"
echo "z='$'(ls -la)"
z=$(ls -l)
echo $z

#rm -r $(cat nomes.txt)

echo "Conclusão, aninhamento de comandos é como se aplicássemos a 'regra distributiva' neles"
