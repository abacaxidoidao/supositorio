#!/bin/bash

vetor[23]="bixoqfoi"
vetor[2]="falavelio"
vetor[7]="voseehbobo?!"

echo "a)"
echo ${vetor[@]}

echo ""

echo "b)"
echo ${vetor[*]}

echo ""

echo "c)"
echo "${vetor[@]}"

echo ""

echo "d)"
echo $vetor[@]

echo ""

echo "Quais os índices preenchidos?"
echo ${!vetor[@]}

echo ""

echo "Qual o número de índices preenchidos?"
echo ${#vetor[@]}


