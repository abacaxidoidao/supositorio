#!/bin/bash

echo "a)"
declare -a vetor
vetor[2]="tekka"

echo ${vetor[2]}

echo ""

echo "b)"
array[35]="oraessa"
echo ${array[35]}

echo ""

echo "c)"
nomes=(Leo Tekka Boneca)
echo ${nomes[1]}

echo ""

echo "d)"
nomes[5]="manocole"
echo ${nomes[5]}

echo ""
