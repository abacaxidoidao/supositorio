#!/bin/bash

echo '
Funçoes com parâmetros:
Ocorre da mesma forma quando passamos argumentos para um script;
São utlizados os parâmetros posicionais $1..$9..{$N};
Também se pode utilizar $# para saber a quantidade de parâmetros passados à funcção, e $@ para saber todos os parâmetros passados;
Pode-se passar variáveis, ${var}, e saída de comandos $(comd), para funções.


maior () 
{
	if (($#>2)); then
		echo "Digie dois números"
		return
	fi

	echo "Você vai comparar $@"
	if (($1>$2)); then
		echo "$1 é maior"
	elif (($1<$2)); then
		echo "$2 é maior"
	else
		echo "São iguais"
	fi


'

maior () 
{
	if (($#>2)); then
		echo "Digie dois números"
		return
	fi

	echo "Você vai comparar $@"
	if (($1>$2)); then
		echo "$1 é maior"
	elif (($1<$2)); then
		echo "$2 é maior"
	else
		echo "São iguais"
	fi

}


maior 2 4 8 -6
maior -17 -19






