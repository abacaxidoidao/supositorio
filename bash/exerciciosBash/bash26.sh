#!/bin/bash

echo '
Chave dupla apresenta outras funcionalidades além da vista anteriormente, porém, vamos relembrar somente sobre variáveis;
Como percebido nos primeiros exemplos, {} delimita o início e o fim do nome da variável, por isso é necessáio seu uso quando se quer valores em vetores e parâmetros
posicionais;
Lembre-se que é boa prática sempre utilizar ${var} independente do tipo de variável;

arquivo="meu texto"
Nesse caso é necessário tambem botar entre aspas duplas a variável
echo "escrevo aqui" > "${arquivo}".txt
cat "${arquivo}".txt
'
arquivo="meu texto"

cat "${arquivo}".txt

echo
echo 'Tente ls ${arquivo}.txt para ver o erro
ls "${arquivo}".txt'
echo


echo "escrevo aqui" > "${arquivo}".txt

echo
ls "${arquivo}".txt



