#!/bin/bash

echo "O comando expr realiza operações matemáticas em scripts, porém é recomendável evitar de utilizar este comando pois
não existe operações com Floating-point values em bash"
echo

echo "x=expr 3 + 2"

x=`expr 3 + 2`
echo $x
echo


echo "y=expr $x + 4"
y=`expr $x + 4`
echo $y

echo "Note o uso da crase que serão explicadas depois (Não aparece no echo)"
