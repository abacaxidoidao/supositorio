#!/bin/bash

echo "Como uma melhor alternativa ao uso do comando expr, temos o comando let e o uso de $/(());
Os dois são bastante semelhantes,com a diferença que let é um comando embutido, logo tem de obedecer a uma sintaxe correta como qualquer comando;
Prefira o uso de $/(())"
echo
echo





echo "let z=3+2"
let z=3+2
echo -e "$z \n"
echo

echo "Espaços são permitidos na atribuição se tiver entre aspas duplas"
echo

echo "let 'z += 10'"
let "z += 10"
echo "let a=z-3"
let a=z-3
echo "let 'b = a / 2'"
let "b = a / 2"
echo
echo "a: $a, b: $b, z: $z"
