#!/bin/bash

echo '
Funções:

Se você perceber que está repetindo um mesmo bloco de código diversas vezes apenas com parâmetros diferentes, então está na hora de criar uma função com esse bloco de 
código;
Como em C, a função deve ser criada antes de sua utilização, caso dentro do mesmo script;

function name {
	CODE
}

Parênteses são opcionais.

Ou:
-----------------------------------------

name () {
	CODE
}

Parênteses são obrigatórios;
Mais portável.

Até agora todas as nossas variáveis criadas eram globais, poderiam ser modificadas em qualquer bloco do script;
Uma variável é por padrão global a menos que seja precedida de local: "local var="eusla"";
Com funções é importante destacar a possibilidade da criação de variáveis locais, que só podem ser acessadas dentro do escopo em que foram definidas.

foo() 
{
	local a="só aqui"
	echo "$a"
}

foo

echo "$a"
'

foo() 
{
	local a="só aqui"
	echo "$a"
}

foo

echo "$a"

