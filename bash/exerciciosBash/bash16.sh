#!/bin/bash

echo "
Comando útil para processamento de texto, semelhante ao awk:
-f: seleciona o campo que se quer;
-c: seleciona os caracteres;
-d: muda o delimitador.

No exemplo seguinte mudamos o delimitador para ':' e selecionamos assim o primeiro e terceiro campo de cada string do arquivo /etc/passwd

cut -d':' -f1,3 /etc/passwd:"
cut -d':' -f1,3 /etc/passwd
