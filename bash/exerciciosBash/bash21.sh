#!/bin/bash

echo -e "
Bash possibilita utilizat os operadores -a e -o, para AND e OR respectivamente, dentro de [];
Também é possível utilizat os operadores conhecidos && e || dentro de [[]].

str=teste
Repare que em [] usa-se \\<

[ -d bashEx -a -v str ]"

str="teste"

[ -d bashEx -a -v str ]
echo $?
echo

echo "[[ -z "letras" || 10 -lt 3 ]]"
[[ -z "letras" || 10 -lt 3 ]]
echo $?
