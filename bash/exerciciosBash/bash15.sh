
#!/bin/bash

echo "Awk é uma linguagem de processamento de texto poderosa, porém será mostrado a parte mais básica, impressão por colunas de uma entrada;
A linguagem separa por colunas o texyo, começando em '$'1 como a primeira coluna, e assim por diante, '$'0 imprime todas as colunas;
'{print}' para printar."
echo
echo

echo "Imprime a segunda coluna:"
echo "Um Dois Tres | awk '{print '$'2}'"
echo
echo Um Dois Tres | awk '{print $2}'
echo


echo "Imprimindo o arquivo inteiro:"
echo "awk '{print '$'0}' ~/Desktop/repoLeo/bash/slidesLinuxBasico/lero.txt"
echo
awk '{print $0}' ~/Desktop/repoLeo/bash/slidesLinuxBasico/lero.txt
