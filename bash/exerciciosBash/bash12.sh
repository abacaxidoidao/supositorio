#!/bin/bash

echo "x=$'(( 3 + (18 / 2)))'"
x=$(( 3 + (18 / 2)))
echo -e "$x \n"

echo "y='$'(( '$'x * 3))"
y=$(( $x * 3))
echo

echo "O uso do $ é facultativo dentro do $/(())"
echo "z=$'(( y - 4))"
z=$(( y - 4))
echo
echo "y: $y, z: $z"
