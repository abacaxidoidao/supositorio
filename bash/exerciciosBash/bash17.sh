#!/bin/bash

echo "
Bash tem uma sintaxe diferente para expressões condicionais relativas a arquivos e números inteiros, o contrário do que se esperaria em uma linguagem de programação;
Elas são utilizadas em conjuto com as estruturas condicionais que serão vistas à frente;
Se utiliza [[]] para testas uma condicção e '$'? para saber se ela é verdadeira ($'?=0) ou falsa ($'?=1);
[[]] é um recurso somente presente em bash, logo não é portável.

Podemos utilizar para checar se arquivos existem, por exemplo:
-e: Verifica se arquivo existe;
-d: Verifica se arquivo existe E é um diretório."
echo

echo "[[ -e velioeusouumarquivoblsa?.txt ]"
[[ -e velioeusouumarquivoblsa?.txt ]]
echo $?

echo

echo "[[ -e bash14.sh ]]"
[[ -e bash14.sh ]]
echo $?

echo

echo "[[ -d Aug ]]"
[[ -d Aug ]]
echo $?

echo

echo "Podemos checar também se uma variável existe, por exemplo:
-v: Checa de variável existe."
echo

echo "str=sei"
str="sei"
echo
echo "[ -v stra ]" 
[ -v stra ]
echo $?
echo

echo "[ -v str ]" 
[ -v str ]
echo $?


