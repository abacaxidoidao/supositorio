#!/bin/bash

echo "
Trabalhando com inteiros:
Para inteiros, usa-se as seguintes opções:
-eq: igual (equal);
-ne: não igual (not equal);
-lt: menor que (less than);
-le: menor que ou igual (less than or equal to);
-gt: maior que (greater than);
-ge: maior que ou igual (greater than or equal to);

As opções antesriores são válidas para inteiros quando se testa uma condição dentro de [[]];
Porém se a condição estiver dentr de (()), pode-se usar os operadoes conhecidos."

echo "[[ 10 > 2 ]]"
[[ 10 > 2 ]]
echo $?
echo

echo "[[ 10 -gt 2 ]]"
[[ 10 -gt 2 ]]
echo $?
echo

echo "
a=10
b=2"
a=10
b=2

echo "(( '$'a > '$'b ))"
(( $a > $b ))
echo $?

