import java.util.Random;

public class Aleatorio {
    private Random rand;
    public Aleatorio() {
        rand = new Random();
        // rand.setSeed(123456789); 
    }

    public int aleatorio(int i) {
        return rand.nextInt(i) + 1;
    }
}