import java.util.List;
import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Dados {
    public static void main(String[] args) {
        Pattern pattern = Pattern.compile("(\\d+)");
        Aleatorio num = new Aleatorio();
        String fp = "input.txt";
        String line = null;
        List<Integer> list = new ArrayList<Integer>();
        int i, sum;

        try {
            FileReader fileReader = new FileReader(fp);
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                Matcher matcher = pattern.matcher(line);
                while (matcher.find()) {
                    list.add(Integer.parseInt(matcher.group()));
                }
                i = 0;
                sum = 0;
                while (i < list.get(0)) {
                    sum += num.aleatorio(list.get(1));
                    i++;
                }
                System.out.println(sum);
                list.removeAll(list);
            }
            bufferedReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Unable to open file '" + fp + "'"); 
        } catch (IOException e) {
            System.out.println("Error reading file '" + fp + "'");     
        }
    }
  
}