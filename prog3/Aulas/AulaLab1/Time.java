import java.util.List;
import java.util.ArrayList;

public class Time {
  private final String nome;
  private final String tecnico;
  private List<Jogador> jogadores;

  public Time(String nome, String tecnico) {
    this.nome = nome;
    this.tecnico = tecnico;
    this.jogadores =  new ArrayList<Jogador>();
  }

  public void adicionaJogador(Jogador jogador){
    jogadores.add(jogador);
  }

  public ArrayList<Jogador> getJogadores(){return new ArrayList<Jogador>(jogadores);}
  public String getNome(){return this.nome;}
  public String getTecnico(){return this.tecnico;}
}