import java.util.ArrayList;

public class Gol implements Tuple<Integer, ArrayList<Time>>{
  private Integer qtdGols;
  private ArrayList<Time> marcadores;

  public Gol(){
    this.qtdGols = new Integer(0);
    this.marcadores = new ArrayList<>();
  }

  public void marcaGol(Time time){
    this.qtdGols++;
    this.marcadores.add(time);
  }

  @Override
  public Integer fst() {
    return new Integer(this.qtdGols);
  }
  @Override
  public ArrayList<Time> snd() {
    return new ArrayList<Time>(this.marcadores);
  }
}