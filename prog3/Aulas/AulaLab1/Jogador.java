public class Jogador{
  private final String nome;
  private final int camisa;

  public Jogador(String nome, int camisa) {
    this.camisa = camisa;
    this.nome = nome;
  }

  public String getNome(){return this.nome;}
  public int getCamisa(){return this.camisa;}
}