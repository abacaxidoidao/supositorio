import java.util.stream.*;

public class Testador{
    public static void main(String[] args) {
      Time vasco = new Time("Vasco", "Moarcir");
      Time flamengo = new Time("Flamengo", "ZehDroguinha");
      PartidaDeFutebol partida = new PartidaDeFutebol(vasco, flamengo);

      IntStream.range(0, 7).forEach(i -> partida.fazGol(vasco));
      partida.fazGol(flamengo);

      partida.placar();
      partida.golsMarcados();
    }
}