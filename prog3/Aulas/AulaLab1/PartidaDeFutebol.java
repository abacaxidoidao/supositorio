import java.util.List;
import java.util.ArrayList;

public class PartidaDeFutebol {
  private final Time time1;
  private final Time time2;
  private Gol gols;

  public PartidaDeFutebol(Time time1, Time time2) {
    this.time1 = time1;
    this.time2 = time2;
    gols = new Gol();
  }
  
  public void placar() {
    int[] golsTime1 = new int[]{0}, golsTime2 = new int[]{0};
    gols.snd().parallelStream().forEach(time -> {
      if (time == time1) golsTime1[0]++;
      else golsTime2[0]++;
    });
    System.out.println(
      this.time1.getNome() + ": " + golsTime1[0] 
      + " vs " + this.time2.getNome() + ": " + golsTime2[0]
    );
  }

  public void golsMarcados(){
    System.out.println("Foram marcados " + gols.fst() + " gols");
  }

  public void fazGol(Time time) {
    gols.marcaGol(time);
  }
}