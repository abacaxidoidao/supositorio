/**
 * Tuple
 */
public interface Tuple<E, G> {
  E fst();
  G snd();
}