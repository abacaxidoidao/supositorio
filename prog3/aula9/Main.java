package aula9;

import aula9.Ponto;
import aula9.Triangulo;

public class Main {
    public static Ponto newPonto(String s1, String s2) {
        double x = Double.parseDouble(s1);
        double y = Double.parseDouble(s2);

        Ponto p = new Ponto(x, y);

        return p;
    }
    
    public static void main(String[] args) {
        Ponto p1 = newPonto(args[0], args[1]);
        Ponto p2 = newPonto(args[2], args[3]);
        Ponto p3 = newPonto(args[3], args[5]);

        Triangulo tri = new Triangulo(p1, p2, p3);
        System.out.println(tri.perim());
    }
    
}