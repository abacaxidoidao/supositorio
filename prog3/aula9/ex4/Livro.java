package loja;

public class Livro extends SuperC {
    private final String autor;
    public Livro(String nome, String autor,double preco) {
        this.nome = nome;
        this.preco = preco;
        this.autor = autor;
    }
    public String toString() {
        return (nome + ", " + preco + ", por " + autor);
    }
}