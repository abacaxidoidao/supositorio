package loja;

public class CD extends SuperC {
    private final int faixas;
    public CD(String nome, double preco, int faixas) {
        this.nome = nome;
        this.preco = preco;
        this.faixas = faixas;
    }

    public String toString() {
        return (nome + ", " + preco + ", " + faixas + " faixas");
    }
    
}