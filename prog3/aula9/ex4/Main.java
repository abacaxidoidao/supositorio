package loja;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        List<Object> loja = new ArrayList<Object>();

        loja.add(new Livro("Oi", "Tchau", 10.00));
        System.out.println(loja.size());
        loja.add(new CD("Machiaveli", 49.99, 16));
        System.out.println(loja.size());
        System.out.println(loja.get(loja.size() - 1));
    }
}