package loja;

public class DVD extends SuperC {
    private int duracao;
    public DVD(String nome, double preco, int duracao) {
        this.nome = nome;
        this.preco = preco;
        this.duracao = duracao;
    }
    public String toString() {
        return (nome + ", " + preco + ", " + duracao + " minutos de duracao");
    }
}