package conta;

public class Conta {
  protected double saldo;
  protected double limite;

    public Conta() {
    }
    public Conta(double limite) {
        this.limite = limite;
    }
    public void deposita(double valor) {
        this.saldo += valor;
    }
    protected void saca(double valor) {
        if (this.saldo + this.limite >= valor) {
            valor += (valor * 0.5) / 100;
            this.saldo -= valor;
        }
        else throw new IllegalArgumentException("Estourou limite");
    }
    public double getSaldo() {
        return this.saldo;
    }    
}