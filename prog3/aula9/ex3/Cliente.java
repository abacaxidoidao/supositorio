package conta;

import conta.Conta;

public class Cliente extends Conta {
    public Cliente(double limite) {
        this.limite = limite;
    }
    public void saca(double valor) {
        if (this.saldo + this.limite >= valor) {
            valor += (valor * 0.1) / 100;
            this.saldo -= valor;
        }
        else throw new IllegalArgumentException("Estourou limite");
    }
    
    
}