package conta;

public class Main {
    public static void main(String[] args) {
        Conta corrente = new Conta(1000);
        Cliente especial = new Cliente(1000);

        corrente.deposita(10);
        System.out.println("Corrente: " +corrente.getSaldo());
        especial.deposita(10);
        System.out.println("Especial: " +especial.getSaldo());
        corrente.saca(5);
        especial.saca(5);
        System.out.println("Corrente: " +corrente.getSaldo());
        System.out.println("Especial: " +especial.getSaldo());
        corrente.saca(5);
        especial.saca(5);
        System.out.println("Corrente: " +corrente.getSaldo());
        System.out.println("Especial: " +especial.getSaldo());
        corrente.saca(1000);
        especial.saca(1000);
    }
    
}