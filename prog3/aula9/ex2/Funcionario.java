// package empresa;

import java.util.Date;
import empresa.Departamento;

public class Funcionario {
    private double salario;
    private final String nome;
    private final Date dataAdmissao;
    
    public Funcionario(double salario, String nome, Date dataAdmissao) {
        this.salario = salario;
        this.nome = nome;
        this.dataAdmissao = dataAdmissao;
    }
    
    public void setSalario(int porcento) {
        double newSalario = ( (this.salario * porcento) / 100);
        this.salario = this.salario + newSalario;
    }

    public double getSalario() {
        return this.salario;
    }
    public String getNome() {
        return this.nome;
    }
    public String getdataAdmissao() {
        return this.dataAdmissao.toString();
    }
}