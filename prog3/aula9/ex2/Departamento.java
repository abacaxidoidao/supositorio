// package empresa;

import java.util.HashSet;

public class Departamento {
    private HashSet<Funcionario> funcionarios;
    private final String nome;

    public Departamento(String nome) {
        this.nome = nome;
        funcionarios = new HashSet<>();
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    public void addFuncionario(Funcionario funcionario) {
        this.funcionarios.add(funcionario);
    }

    public void giveRaise(int aumento) {
        funcionarios.parallelStream()
            .forEach(funcionario -> funcionario.setSalario(aumento));        
    }
}