// package empresa;

import java.util.Date;

public class Main {
    public static void main(String[] args) {
        Funcionario f1 = new Funcionario(1000, "leo", new Date() );
        Funcionario f2 = new Funcionario(1000, "leleo", new Date() );
        Funcionario f3 = new Funcionario(1000, "lheo", new Date() );

        Departamento d1 = new Departamento("casa");
        d1.addFuncionario(f1);
        d1.addFuncionario(f2);
        d1.addFuncionario(f3);

        Empresa empresa = new Empresa("cnpj");
        empresa.addDepartamento(d1);
        
        System.out.println(f1.getSalario());
        d1.giveRaise(10);
        System.out.println(f1.getSalario());
    }
    
}