// package empresa;

import java.util.HashSet;

public class Empresa {
    private HashSet<Departamento> departamentos;
    private final String cnpj;

    public Empresa(String cnpj) {
        this.cnpj = cnpj;
        this.departamentos = new HashSet<>();
    }
    
    /**
     * @return the cnpj
     */
    public String getCnpj() {
        return this.cnpj;
    }

    public void addDepartamento(Departamento departamento) {
        this.departamentos.add(departamento);
    }

    public HashSet<Departamento> getDepartamentos() {
        return new HashSet<Departamento>(this.departamentos);
    }
}