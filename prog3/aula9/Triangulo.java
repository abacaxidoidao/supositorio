package aula9;

import aula9.Ponto;

public class Triangulo {
    private Ponto p1;
    private Ponto p2;
    private Ponto p3;
    private double perim;

    public Triangulo(Ponto p1, Ponto p2, Ponto p3) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
    }

    private Ponto[] getPoints() {
        Ponto[] pointArr = new Ponto[3];
        pointArr[0] = this.p1;
        pointArr[1] = this.p2;
        pointArr[2] = this.p3;

        return pointArr;
    }

    public double perim() {
        Ponto[] pointArr = getPoints();

        double dist1 = Math.abs( Math.sqrt( Math.pow(pointArr[0].getX() - pointArr[1].getX(), 2) + Math.pow(pointArr[0].getY() - pointArr[1].getY(), 2) ) );
        double dist2 = Math.abs( Math.sqrt( Math.pow(pointArr[0].getX() - pointArr[2].getX(), 2) + Math.pow(pointArr[0].getY() - pointArr[2].getY(), 2) ) );
        double dist3 = Math.abs( Math.sqrt( Math.pow(pointArr[2].getX() - pointArr[1].getX(), 2) + Math.pow(pointArr[2].getY() - pointArr[1].getY(), 2) ) );

        return dist1 + dist2 + dist3;
    }
}